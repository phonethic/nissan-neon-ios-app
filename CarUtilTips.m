//
//  CarUtilTips.m
//  CarUtil
//
//  Created by Sagar Mody on 20/01/13.
//  Copyright (c) 2013 Phonethics. All rights reserved.
//

#import "CarUtilTips.h"

@implementation CarUtilTips
@synthesize tipsid,ilink,vlink;

-(id)initWithID:(int)ltipsid image:(NSString *)lilink  video:(NSString *)lvlink
{
    self = [super init];
    
    if(self) {
        self.tipsid = ltipsid;
        self.ilink = lilink;
        self.vlink = lvlink;
    }
	return self;
}

-(void)dealloc
{
    self.ilink = nil;
    self.vlink = nil;
    [super dealloc];
}
@end
