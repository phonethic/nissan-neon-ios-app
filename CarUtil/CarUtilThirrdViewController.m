//
//  CarUtilThirrdViewController.m
//  CarUtil
//
//  Created by Rishi Saxena on 29/03/12.
//  Copyright (c) 2012 Phonethics. All rights reserved.
//

#import "CarUtilThirrdViewController.h"
#import "CarUtilAppDelegate.h"
#import "MaintenanceViewController.h"
#import "SafteyViewController.h"
#import "DrivingTipsViewController.h"
#import "LocationTipsViewController.h"
#import "SeasonTipsViewController.h"
#import "HowtoVideoViewController.h"
#import "UIImageView+WebCache.h"
#import "CarUtilTips.h"
#import "CarUtilAppDelegate.h"
#import "MKNumberBadgeView.h"
#import "CarUtilTipsSectionViewController.h"

#define NEON_TIPS_LINK @"http://stage.phonethics.in/proj/neon/neon_tips.php"
//#define NEON_TIPS_LINK @"http://localhost/carutil/neon_tips.xml"

@interface CarUtilThirrdViewController ()

@end

@implementation CarUtilThirrdViewController
@synthesize loadingImageView;
@synthesize listData;
@synthesize tipstbl;
@synthesize tipsdata;
@synthesize sectionName;
@synthesize tipsArray;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        self.navigationItem.title = NSLocalizedString(@"Tips", @"Tips");
        //self.tabBarItem.image = [UIImage imageNamed:@"tips"];
        //[self setMyTitle:@"Tips"];
    }
    return self;
}
- (void)setMyTitle:(NSString *)title
{
    [super setTitle:title];
    UILabel *titleView = (UILabel *)self.navigationItem.titleView;
    if (!titleView) {
        titleView = [[UILabel alloc] initWithFrame:CGRectZero];
        titleView.backgroundColor = [UIColor clearColor];
        titleView.font = [UIFont boldSystemFontOfSize:20.0];
        titleView.shadowColor = [UIColor colorWithWhite:0.0 alpha:0.5];
        
        titleView.textColor = [UIColor orangeColor]; // Change to desired color
        
        self.navigationItem.titleView = titleView;
        [titleView release];
    }
    titleView.text = title;
    [titleView sizeToFit];
}

- (void)viewDidLoad
{
#ifdef ENABLE_BACK_BUTTON
    UIBarButtonItem *backButton = [[UIBarButtonItem alloc] initWithTitle:@"Back" style:UIBarButtonItemStylePlain target:nil action:nil];
    self.navigationItem.backBarButtonItem = backButton;
    [backButton release];
    
#endif
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    DebugLog(@"viewDidLoad");
    [Flurry logEvent:@"Tips_Tab_Event"];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
#ifdef TESTING
    [TestFlight passCheckpoint:@"Tips Tab Pressed"];
#endif
    if([NEONAppDelegate networkavailable]) {
        [self tipsListAsynchronousCall];
    }  else  {
        [self parseFromFile];
    }
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    DebugLog(@"nissan viewDidAppear");
    DebugLog(@"viewDidAppear");
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    DebugLog(@"Tips Tab nissan viewWillDisappear");
    MKNumberBadgeView *tempbadge = (MKNumberBadgeView *)[self.tabBarController.tabBar viewWithTag:20];
    tempbadge.value = 0;
    [NEONAppDelegate setBadgeValue1:0];
    [UIApplication sharedApplication].applicationIconBadgeNumber = 0;
}


#pragma mark -
#pragma mark Table View Data Source Methods
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [tipsdata count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *SimpleTableIdentifier = @"SimpleTableIdentifier";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:
                             SimpleTableIdentifier];
    if (cell == nil) {
        cell = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:SimpleTableIdentifier] autorelease];
    }
    
    //    UIImage *image = [UIImage imageNamed:@"star.png"];
    //    cell.imageView.image = image;
    //    UIImage *image2 = [UIImage imageNamed:@"star2.png"];
    //    cell.imageView.highlightedImage = image2;
    
    NSUInteger row = [indexPath row];
    cell.textLabel.text = [[tipsdata allKeys] objectAtIndex:row];
    cell.textLabel.textColor = [UIColor whiteColor];
    cell.textLabel.font = [UIFont boldSystemFontOfSize:17.0];
    //    cell.selectionStyle = UITableViewCellSelectionStyleNone;;
    cell.textLabel.shadowColor = [UIColor blackColor];
	cell.textLabel.shadowOffset = CGSizeMake(0, 1);
    cell.textLabel.backgroundColor = [UIColor clearColor];
    //cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    
    UIImageView *bg = [[UIImageView alloc] initWithFrame:cell.frame];
	bg.image = [UIImage imageNamed:@"tips_bg_off.png"];
	cell.backgroundView = bg;
	[bg release];
    UIImageView *bgon = [[UIImageView alloc] initWithFrame:cell.frame];
	bgon.image = [UIImage imageNamed:@"tips_bg.png"];
	cell.selectedBackgroundView = bgon;
	[bgon release];
    
    return cell;
}

#pragma mark -
#pragma mark Table Delegate Methods
//
//- (NSInteger)tableView:(UITableView *)tableView indentationLevelForRowAtIndexPath:(NSIndexPath *)indexPath {
//    NSUInteger row = [indexPath row];
//    return row;
//}

-(NSIndexPath *)tableView:(UITableView *)tableView  willSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    NSUInteger row = [indexPath row];
    
    if (row == -1)
        return nil;
    
    return indexPath;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
  
    NSString *key = [[tipsdata allKeys] objectAtIndex:indexPath.row];
    DebugLog(@"selected catg %@ ",key);
    
    NSMutableArray *tempArray = [tipsdata objectForKey:[[tipsdata allKeys] objectAtIndex:indexPath.row]];
    DebugLog(@"selected array %@ ",tempArray);
    
    CarUtilTipsSectionViewController *maintenanceController = [[CarUtilTipsSectionViewController alloc] initWithNibName:@"CarUtilTipsSectionViewController" bundle:nil] ;
    maintenanceController.title = key;
    NSSortDescriptor *alphaDesc = [[NSSortDescriptor alloc] initWithKey:@"tipsid" ascending:YES selector:@selector(compare:)];
    [tempArray sortUsingDescriptors:[NSMutableArray arrayWithObjects:alphaDesc, nil]];
    //to push the UIView.
    maintenanceController.tipsObjsArray = tempArray;
    maintenanceController.hidesBottomBarWhenPushed = NO;
    [self.navigationController pushViewController:maintenanceController animated:YES];
    [maintenanceController release];
    [alphaDesc release]; alphaDesc = nil;
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    CGRect screenBounds = [[UIScreen mainScreen] bounds];
    if (screenBounds.size.height == 568) {
        // code for 4-inch screen
        return 76;
    } else {
        // code for 3.5-inch screen
        return 60;
    }
    return 60;
}

-(void)tipsListAsynchronousCall
{
	/****************Asynchronous Request**********************/
	NSURLRequest *urlRequest = [NSURLRequest requestWithURL:[NSURL URLWithString:NEON_TIPS_LINK] cachePolicy:YES timeoutInterval:10.0];
    
	// Note: An NSOperation creates an autorelease pool, but doesn't schedule a run loop
	// Create the connection and schedule it on a run loop under our namespaced run mode
	NSURLConnection *theConnection = [[NSURLConnection alloc] initWithRequest:urlRequest delegate:self startImmediately:YES];
    [theConnection autorelease];
	
}

#pragma mark NSURLConnection Delegate Methods

- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response {
	
	if ([response isKindOfClass:[NSHTTPURLResponse class]])
	{
        NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *) response;
        
		status = [httpResponse statusCode];
	}
}

- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data {
	
	if(responseAsyncData==nil)
	{
		responseAsyncData = [[NSMutableData alloc] initWithLength:0];
	}
    
	[responseAsyncData appendData:data];
}

- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error {
	[self connectionDidFinishLoading:nil];
    //[self parseFromFile];
    DebugLog(@"Error: %@", [error localizedDescription]);
}

- (void)connectionDidFinishLoading:(NSURLConnection *)connection
{
	NSString *xmlDataFromChannelSchemes;
    
	if(responseAsyncData)
	{
		NSString *result = [[NSString alloc] initWithData:responseAsyncData encoding:NSASCIIStringEncoding];
		//DebugLog(@"\n result:%@\n\n", result);
        [NEONAppDelegate writeToTextFile:result name:@"tips"];
		xmlDataFromChannelSchemes = [[NSString alloc] initWithString:result];
		NSData *xmlDataInNSData = [xmlDataFromChannelSchemes dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];
		xmlParser = [[NSXMLParser alloc] initWithData:xmlDataInNSData];
		[xmlParser setDelegate:self];
		[xmlParser parse];
        [xmlParser release];
		[xmlDataFromChannelSchemes release];
		[result release];
		[responseAsyncData release];
		responseAsyncData = nil;
	}
    else {
        [self parseFromFile];
    }
}

-(void) parseFromFile
{
    NSString *xmlDataFromChannelSchemes;
    NSString *data;
    data = [NEONAppDelegate getTextFromFile:@"tips"];
    DebugLog(@"\n data:%@\n\n", data);
    if(data != nil && ![data isEqualToString:@""])
    {
        xmlDataFromChannelSchemes = [[NSString alloc] initWithString:data];
        NSData *xmlDataInNSData = [xmlDataFromChannelSchemes dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];
        xmlParser = [[NSXMLParser alloc] initWithData:xmlDataInNSData];
        [xmlParser setDelegate:self];
        [xmlParser parse];
        [xmlParser release];
        [xmlDataFromChannelSchemes release];
    }
    else {
        UIAlertView *errorView = [[[UIAlertView alloc]
                                  initWithTitle: ALERT_TITLE
                                  message:@"Please check your internet connection and try again."
                                  delegate:self
                                  cancelButtonTitle:@"OK"
                                  otherButtonTitles:nil] autorelease];
        [errorView show];
    }

}

#pragma mark xmlParser methods
/* Called when the parser runs into an open tag (<tag>) */
- (void)parser:(NSXMLParser *)parser didStartElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qualifiedName 	attributes:(NSDictionary *)attributeDict
{
	if([elementName isEqualToString:@"tipsgallery"])
	{
        tipsdata = [[NSMutableDictionary alloc] init];
	} else if([elementName isEqualToString:@"section"]) {

        sectionName = [[NSString alloc] initWithString:[attributeDict objectForKey:@"name"]];
        tipsArray = [[NSMutableArray alloc] init];
    } else if([elementName isEqualToString:@"photo"]) {
        tipsObj = [[CarUtilTips alloc] init];
        tipsObj.tipsid = [[attributeDict objectForKey:@"id"] intValue];
        tipsObj.ilink = [attributeDict objectForKey:@"ilink"];
        tipsObj.vlink = [attributeDict objectForKey:@"vlink"];
        //DebugLog(@"%@ %@ %@",[attributeDict objectForKey:@"id"],[attributeDict objectForKey:@"ilink"],[attributeDict objectForKey:@"vlink"]);
    }
}

-(void)parser:(NSXMLParser *)parser foundCharacters:(NSString *)string {
    DebugLog(@"Processing Value: %@", string);
    //[ElementValue appendString:string];
}

/* Called when the parser runs into a close tag (</tag>). */
- (void)parser:(NSXMLParser *)parser didEndElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName
{
    if ([elementName isEqualToString:@"tipsgallery"]) {
        DebugLog(@"tipsdata count %d",[tipsdata count]);
        [tipstbl reloadData];
        //DebugLog(@"%@", tipsdata);
        
    } else if([elementName isEqualToString:@"section"]) {
        [tipsdata setObject:tipsArray forKey:sectionName];
        [tipsArray release];
        tipsArray = nil;
        [sectionName release];
        sectionName = nil;
    } else if([elementName isEqualToString:@"photo"]) {
        [tipsArray addObject:tipsObj];
        [tipsObj release];
        tipsObj = nil;
    }
}

- (void)parser:(NSXMLParser *)parser parseErrorOccurred:(NSError *)parseError
{
    DebugLog(@"Error: %@", [parseError localizedDescription]);
}

- (void)parser:(NSXMLParser *)parser validationErrorOccurred:(NSError *)validationError
{
    DebugLog(@"Error: %@", [validationError localizedDescription]);
}

- (void)viewDidUnload
{
    self.listData = nil;
    [self setTipstbl:nil];
    [self setLoadingImageView:nil];
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return YES;
}
-(BOOL)shouldAutorotate
{
    return NO;
}

-(NSUInteger)supportedInterfaceOrientations
{
    return UIInterfaceOrientationMaskPortrait;
}
- (void)dealloc {
    [listData release];
    [tipstbl release];
    [tipsdata release];
    [loadingImageView release];
    [super dealloc];
}
@end
