//
//  AddEcditFuelViewController.h
//  CarUtil
//
//  Created by Rishi Saxena on 19/05/12.
//  Copyright (c) 2012 Phonethics. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <sqlite3.h>
#define START 0
#define INTERMEDIATE 1
#define END 2

@interface AddEcditFuelViewController : UIViewController <UIActionSheetDelegate>
{
    UIActionSheet *actionSheet;
    sqlite3 *expenseDB;
    double datedoubleval;
    int isPartial;
}
@property (readwrite, nonatomic) NSInteger carId;
@property (readwrite, nonatomic) NSInteger dbowID;
@property (readwrite, nonatomic) double minodometerValue;
@property (retain, nonatomic) IBOutlet UIButton *dateBtn;
@property (retain, nonatomic) IBOutlet UIScrollView *scrollView;
@property (readwrite, nonatomic) int headerImageType;
@property (retain, nonatomic) IBOutlet UIImageView *headerImageView;
@property (retain, nonatomic) IBOutlet UILabel *fuellbl;
@property (retain, nonatomic) IBOutlet UILabel *tankFullBtnlbl;
@property (retain, nonatomic) IBOutlet UILabel *costlbl;

@property (retain, nonatomic) IBOutlet UITextField *odometerText;
@property (retain, nonatomic) IBOutlet UITextField *fuelText;
@property (retain, nonatomic) IBOutlet UIButton *tankFullBtn;
@property (retain, nonatomic) IBOutlet UITextField *costText;
@property (retain, nonatomic) IBOutlet UIDatePicker *fueldatepicker;
@property (retain, nonatomic) IBOutlet UIButton *tankFullHelpBtn;

- (IBAction)dateBtnPressed:(id)sender;
- (IBAction)tankFullBtnPressed:(id)sender;
- (IBAction)tankFullHelpBtnPressed:(id)sender;

@end
