//
//  MielageDetail.m
//  CarUtil
//
//  Created by Rishi Saxena on 18/05/12.
//  Copyright (c) 2012 Phonethics. All rights reserved.
//

#import "MielageDetail.h"

@implementation MielageDetail

@synthesize startdate,enddate,startreading,endreading,avgMileage,cost,average,mileageid;
@synthesize selected,flagType;

-(id)initWithID:(NSInteger)lid  startdate:(double)sdate enddate:(double)edate startreading:(double)sreading endreading:(double)ereading avgMileage:(double)lavgMileage average:(double)lavg cost:(double)lcost selected:(NSString *)lselected flagType:(NSInteger)lflagType

{
    self = [super init];
    
    if(self) {
        self.mileageid = lid;
        self.startdate = sdate;
        self.enddate = edate;
        self.startreading = sreading;
        self.endreading = ereading;
        self.avgMileage = lavgMileage;
        self.average = lavg;
        self.cost = lcost;
        self.selected = lselected;
        self.flagType = lflagType;
    }
	return self;
}

-(void)dealloc
{
    //[selected release];
    self.selected = nil;
    [super dealloc];
}

@end
