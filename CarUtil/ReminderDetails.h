//
//  ReminderDetails.h
//  CarUtil
//
//  Created by Rishi Saxena on 24/05/12.
//  Copyright (c) 2012 Phonethics. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ReminderDetails : NSObject {
    NSInteger reminderdetailid;
    NSInteger reminderdeOnOff;
    NSString *message;
    double date;
    NSInteger completed;
    NSInteger prevalue;
    NSString *pretype;
}
@property (nonatomic, readwrite) NSInteger reminderdetailid;
@property (nonatomic, readwrite) NSInteger reminderdeOnOff;
@property (nonatomic, copy) NSString *message;
@property (nonatomic, readwrite) double date;
@property (nonatomic, readwrite) NSInteger completed;
@property (nonatomic, readwrite) NSInteger prevalue;
@property (nonatomic, copy) NSString *pretype;

-(id)initWithID:(NSInteger)lreminderdetailid remindOnOff:(NSInteger)lreminderdeOnOff  message:(NSString *)lmessage date:(double)ldate completed:(NSInteger)lcompleted prevalue:(NSInteger)lpreval pretype:(NSString *)lpretype;

@end
