//
//  ReminderDetails.m
//  CarUtil
//
//  Created by Rishi Saxena on 24/05/12.
//  Copyright (c) 2012 Phonethics. All rights reserved.
//

#import "ReminderDetails.h"

@implementation ReminderDetails 

@synthesize reminderdetailid;
@synthesize reminderdeOnOff;
@synthesize message;
@synthesize date;
@synthesize completed;
@synthesize pretype;
@synthesize prevalue;

-(id)initWithID:(NSInteger)lreminderdetailid remindOnOff:(NSInteger)lreminderdeOnOff  message:(NSString *)lmessage date:(double)ldate completed:(NSInteger)lcompleted prevalue:(NSInteger)lpreval pretype:(NSString *)lpretype
{
    self = [super init];
    
    if(self) {
        self.reminderdetailid = lreminderdetailid;
        self.reminderdeOnOff = lreminderdeOnOff;
        self.message = lmessage;
        self.date = ldate;
        self.completed = lcompleted;
        self.prevalue = lpreval;
        self.pretype = lpretype;
    }
    return self;
}

-(void)dealloc
{
    self.message = nil;
    self.pretype = nil;
    [super dealloc];
}

@end
