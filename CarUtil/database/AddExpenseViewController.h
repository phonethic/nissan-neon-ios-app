//
//  AddExpenseViewController.h
//  sqllite sample
//
//  Created by Rishi Saxena on 24/04/12.
//  Copyright (c) 2012 Phonethics. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <sqlite3.h>
#import <QuartzCore/QuartzCore.h>

#define EXPENSE_TYPE_TAG 2000
#define EXPENSE_DATE_TAG 2001
@interface AddExpenseViewController : UIViewController <UITextFieldDelegate,UITextViewDelegate,UITableViewDelegate, UITableViewDataSource,UIScrollViewDelegate,UIActionSheetDelegate,UIPickerViewDataSource,UIPickerViewDelegate> {
    UITextField *datetextField;
    UITextField *costtextField;
    UITextField *odometertextField;
    sqlite3 *expenseDB;
    UIButton *dateBut;
    double datedoubleval;
     NSInteger carID;
    //Drop down list
    IBOutlet UITableView *tblSimpleTable;
	IBOutlet UIButton *btn;
	IBOutlet UIImageView *i;
	BOOL flag;
	NSMutableArray *arryData;
    UIActionSheet *actionSheet;
    int CURRENTCASE;
}

@property (nonatomic, retain) IBOutlet UITextField *datetextField;
@property (nonatomic, retain) IBOutlet UITextView *notestextview;
@property (nonatomic, retain) IBOutlet UITextField *costtextField;
@property (nonatomic, retain) IBOutlet UITextField *odometertextField;
@property (retain, nonatomic) IBOutlet UIButton *dateBut;
@property (retain, nonatomic) IBOutlet UILabel *datelabel;
@property (retain, nonatomic) IBOutlet UIScrollView *scrollviewP;
@property (retain, nonatomic) IBOutlet UIButton *flipbut;
@property (readwrite, nonatomic) double datedoubleval;
@property (readwrite, nonatomic) NSInteger carID;
//Drop down list
@property (retain, nonatomic) IBOutlet UITextField *exptypetextField;
@property(nonatomic,retain)IBOutlet UITableView *tblSimpleTable;
@property(nonatomic,retain)IBOutlet UIImageView *i;
@property(nonatomic,retain)NSMutableArray *arryData;
@property (retain, nonatomic) IBOutlet UIPickerView *exptypepicker;
@property (retain, nonatomic) IBOutlet UIDatePicker *expensedatePicker;
//-(void) readExpenseFromDatabase ;
//- (IBAction)insertData:(id)sender;
- (IBAction)enterDate:(id)sender;
- (IBAction)enterOdometerValue:(id)sender;

-(void)setOdometerFieldValue:(NSString *) value;
-(void)setDateFieldValue:(NSString *) value doubleval:(double)val type:(int)ltype;
@end
