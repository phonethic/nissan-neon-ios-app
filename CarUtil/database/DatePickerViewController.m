//
//  DatePickerViewController.m
//  sqllite sample
//
//  Created by Rishi Saxena on 26/04/12.
//  Copyright (c) 2012 Phonethics. All rights reserved.
//

#import "DatePickerViewController.h"
#import "AddExpenseViewController.h"

@interface DatePickerViewController ()

@end

@implementation DatePickerViewController
@synthesize datePicker;
@synthesize datePickType;
@synthesize pickerdatelbl;
@synthesize datedoubleval;
@synthesize pickerMainView;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    //datePicker.date = [NSDate dateWithTimeIntervalSinceReferenceDate:357303011.000000];
    UIBarButtonItem *SaveButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemSave target:self action:@selector(Savedata:)];
    self.navigationItem.rightBarButtonItem = SaveButton;
    [SaveButton release];
    if(0 == (int)datedoubleval) {
        [datePicker setDate:[NSDate date] animated:YES];
        [datePicker sizeToFit];
        [self pickerdateChanged:nil];
    } else {
        [self todo:datedoubleval];
    }
    [datePicker addTarget:self action:@selector(pickerdateChanged:) forControlEvents:UIControlEventValueChanged];
//    CGSize pickerSize = [datePicker sizeThatFits:CGSizeZero];
//    UIView *pickerTransformView;
//    pickerTransformView = [[UIView alloc] initWithFrame:CGRectMake(0.0f, 0.0f, pickerSize.width, pickerSize.height)];
//    pickerTransformView.transform = CGAffineTransformMakeScale(0.90f, 0.90f);
//    
//    [pickerTransformView addSubview:datePicker];
//    [self.view addSubview:pickerTransformView];
//    [pickerTransformView release];
    CGRect screenBounds = [[UIScreen mainScreen] bounds];
    if (screenBounds.size.height == 568) {
        // code for 4-inch screen
        datePicker.frame = CGRectMake(datePicker.frame.origin.x, datePicker.frame.origin.y + 90 , datePicker.frame.size.width,  datePicker.frame.size.height);
        pickerMainView.frame = CGRectMake(pickerMainView.frame.origin.x, pickerMainView.frame.origin.y + 90 , pickerMainView.frame.size.width,  pickerMainView.frame.size.height);
    }

}

- (void) pickerdateChanged:(id)sender{
    // handle date changes
    NSDate *pickerDate = [datePicker date]; 
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat:@"dd MMMM yyyy"];
    NSString *dateString = [dateFormat stringFromDate:pickerDate];  
    [dateFormat release];
    pickerdatelbl.text = dateString;
    
}

-(void)todo :(double)ldate {
    NSDate *startDate = [NSDate dateWithTimeIntervalSinceReferenceDate:ldate]; 
    NSDateFormatter *sdateFormat = [[NSDateFormatter alloc] init];
    [sdateFormat setDateFormat:@"dd MMMM yyyy"];
    NSString *sdateString = [sdateFormat stringFromDate:startDate];  
    pickerdatelbl.text = sdateString;
    [sdateFormat release];
    [datePicker setDate:startDate animated:YES];

}

- (void) Savedata: (id) sender {
    NSDate *pickerDate = [datePicker date]; 
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    if([self.title isEqualToString:@"Edit Start Date"] || [self.title isEqualToString:@"Start Date"] || [self.title isEqualToString:@"Edit End Date"] || [self.title isEqualToString:@"End Date"]) {
        [dateFormat setDateFormat:@"dd MMM yyyy"];
    } else {
         [dateFormat setDateFormat:@"dd MMMM yyyy"];
    }
    NSString *dateString = [dateFormat stringFromDate:pickerDate];  
    [dateFormat release];
    double temp  = [pickerDate timeIntervalSinceReferenceDate];
    AddExpenseViewController *parent = [self.navigationController.viewControllers objectAtIndex:2];
    //call the method on the parent view controller
    if(parent != nil) 
        [parent setDateFieldValue:dateString doubleval:temp type:datePickType];
    parent = nil;
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)viewDidUnload
{
    [self setDatePicker:nil];
    [self setPickerdatelbl:nil];
    [self setPickerMainView:nil];
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
   
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

-(BOOL)shouldAutorotate
{
    return NO;
}

-(NSUInteger)supportedInterfaceOrientations
{
    return UIInterfaceOrientationMaskPortrait;
}

- (void)dealloc {
    [datePicker release];
    [pickerdatelbl release];
    [pickerMainView release];
    [super dealloc];
}
@end
