//
//  Expense.m
//  sqllite sample
//
//  Created by Rishi Saxena on 30/04/12.
//  Copyright (c) 2012 Phonethics. All rights reserved.
//

#import "Expense.h"

@implementation Expense
@synthesize expenseid, date, cost, type, notes, reading, selected;

-(id)initWithID:(NSInteger)lid  date:(NSString *)ldate cost:(NSString *)lcost type:(NSString *)ltype notes:(NSString *)lnotes reading:(NSString *)lreading selected:(NSString *)lselected
{
   self = [super init];
    
    if(self) {
        self.expenseid = lid;
        self.date = ldate;
        self.cost = lcost;
        self.type = ltype;
        self.notes = lnotes;
        self.reading = lreading;
        self.selected = lselected;
    }
	return self;
}

-(void)dealloc
{
    self.date = nil;
    self.cost = nil;
    self.type = nil;
    self.notes = nil;
    self.reading = nil;
    self.selected =nil;
    [super dealloc];
}

@end
