//
//  ExpenseTypeDetailViewController.h
//  CarUtil
//
//  Created by Rishi Saxena on 02/05/12.
//  Copyright (c) 2012 Phonethics. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <sqlite3.h>

@interface ExpenseTypeDetailViewController : UIViewController <UITableViewDelegate, UITableViewDataSource> {
    IBOutlet UITableView *typeDetailTable;
	NSMutableArray *arryExpType;
    //NSString *databasePath;
    sqlite3 *expenseDB;
    UIView *headerView;
    UIView *footerView;
}
@property(nonatomic,retain)  UIView *headerView;
@property(nonatomic,retain)  UIView *footerView;
@property(nonatomic,retain)IBOutlet UITableView *typeDetailTable;
@property(nonatomic,retain)NSMutableArray *arryExpType;

-(void) reloadExpenseType;

@end
