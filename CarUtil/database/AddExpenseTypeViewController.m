//
//  AddExpenseTypeViewController.m
//  CarUtil
//
//  Created by Rishi Saxena on 02/05/12.
//  Copyright (c) 2012 Phonethics. All rights reserved.
//

#import "AddExpenseTypeViewController.h"
#import "ExpenseTypeDetailViewController.h"
#import "CarUtilAppDelegate.h"

@interface AddExpenseTypeViewController ()

@end

@implementation AddExpenseTypeViewController
@synthesize typeView;
@synthesize value;
@synthesize expTypeid;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    UIBarButtonItem *upexptypeButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemSave target:self action:@selector(AddUpdateExpenseType:)];
    self.navigationItem.rightBarButtonItem = upexptypeButton;
    [upexptypeButton release];
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    [typeView becomeFirstResponder];
    if(value != nil && self.expTypeid != -1)
        typeView.text = value;
}



- (void) AddUpdateExpenseType: (id) sender {
    if(self.expTypeid != -1) 
    {
        [self UpdateExpenseType];
    } else {
        [self AddExpenseType];
    }
}

- (void) UpdateExpenseType
{
    sqlite3_stmt    *statement;
    int typeID;
    
    const char *dbpath = [[(CarUtilAppDelegate *)[[UIApplication sharedApplication] delegate] databasePath] UTF8String];
    
    if (sqlite3_open(dbpath, &expenseDB) == SQLITE_OK)
    {
        NSString *updateSQL = [NSString stringWithFormat:@"update EXPTYPE set TYPE=\"%@\"  where id=\"%d\"",  typeView.text , self.expTypeid];
        const char *update_stmt = [updateSQL UTF8String];
        sqlite3_prepare_v2(expenseDB, update_stmt, -1, &statement, NULL);
        if (sqlite3_step(statement) == SQLITE_DONE)
        {
            typeID = sqlite3_last_insert_rowid(expenseDB);
            DebugLog(@"%d",typeID);
        } else {
        }
        sqlite3_finalize(statement);
    } 
    sqlite3_close(expenseDB);

    ExpenseTypeDetailViewController *parent = [self.navigationController.viewControllers objectAtIndex:3];
    //call the method on the parent view controller
    //[parent adddataInExpenseTypeArray:typeID type:typeView.text];
    if(parent != nil) 
        [parent reloadExpenseType];
    //[self clearData:nil];
    parent = nil;
    [self.navigationController popViewControllerAnimated:YES];
}

- (void) AddExpenseType
{
    sqlite3_stmt    *statement;
    int typeID;
    
    const char *dbpath = [[(CarUtilAppDelegate *)[[UIApplication sharedApplication] delegate] databasePath] UTF8String];
    
    if (sqlite3_open(dbpath, &expenseDB) == SQLITE_OK)
    {
        NSString *insertSQL = [NSString stringWithFormat:@"INSERT INTO EXPTYPE (TYPE) VALUES (\"%@\")", typeView.text];
        const char *insert_stmt = [insertSQL UTF8String];
        sqlite3_prepare_v2(expenseDB, insert_stmt, -1, &statement, NULL);
        if (sqlite3_step(statement) == SQLITE_DONE)
        {
            typeID = sqlite3_last_insert_rowid(expenseDB);
            DebugLog(@"%d",typeID);
        } else {
        }
        sqlite3_finalize(statement);
    } 
    sqlite3_close(expenseDB);

    ExpenseTypeDetailViewController *parent = [self.navigationController.viewControllers objectAtIndex:3];
    //call the method on the parent view controller
    //[parent adddataInExpenseTypeArray:typeID type:typeView.text];
    if(parent != nil) 
        [parent reloadExpenseType];
    //[self clearData:nil];
    parent = nil;
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)viewDidUnload
{
    typeView = nil;
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}


- (void)dealloc {
    [typeView release];
    [value release];
    [super dealloc];
}


- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

-(BOOL)shouldAutorotate
{
    return NO;
}

-(NSUInteger)supportedInterfaceOrientations
{
    return UIInterfaceOrientationMaskPortrait;
}

@end
