//
//  OdometerPickerViewController.m
//  sqllite sample
//
//  Created by Rishi Saxena on 26/04/12.
//  Copyright (c) 2012 Phonethics. All rights reserved.
//

#import "OdometerPickerViewController.h"
#import "AddExpenseViewController.h"
//#import "AddMileageViewController.h"

@interface OdometerPickerViewController ()

@end

@implementation OdometerPickerViewController
@synthesize odometerPicker;
@synthesize valueTypes;
@synthesize readingLabel;
@synthesize setvalue;
@synthesize parentControllerType;
@synthesize pickerMainView;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    UIBarButtonItem *rightBarButtonItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemSave target:self action:@selector(saveData)];
    self.navigationItem.rightBarButtonItem = rightBarButtonItem;
    [rightBarButtonItem release];
    
    NSArray *breadArray = [[NSArray alloc] initWithObjects:@"0",@"1",@"2",@"3",@"4",@"5",@"6",@"7",@"8",@"9", nil];
   	self.valueTypes = breadArray;
	[breadArray release];
    
    readingLabel.text = self.setvalue;
    //DebugLog(@" val = -%@- ,,,, i = %d", self.setvalue, self.setvalue.length);
    int component = 6;
    if(![self.setvalue isEqualToString:@"0"]) {
        for(int i=self.setvalue.length - 1;i>=0;i--)
        {
            //DebugLog(@"i = %d ,,,, char = %c", i ,[setvalue characterAtIndex:i]);
            if([self.setvalue characterAtIndex:i] != ',' && [self.setvalue characterAtIndex:i] != '.') {
                //DebugLog(@"i = %d ,,,, char = %c", i ,[self.setvalue characterAtIndex:i]);
                [odometerPicker selectRow:[self.setvalue characterAtIndex:i] - 48 inComponent:component animated:YES];
                [odometerPicker reloadComponent:component];
                component --;
                
            }
        }
    }
    
    CGRect screenBounds = [[UIScreen mainScreen] bounds];
    if (screenBounds.size.height == 568) {
        // code for 4-inch screen
        odometerPicker.frame = CGRectMake(odometerPicker.frame.origin.x, odometerPicker.frame.origin.y + 90 , odometerPicker.frame.size.width,  odometerPicker.frame.size.height);
        pickerMainView.frame = CGRectMake(pickerMainView.frame.origin.x, pickerMainView.frame.origin.y + 90 , pickerMainView.frame.size.width,  pickerMainView.frame.size.height);
    }
}

-(void)saveData
{
    if(self.parentControllerType == 1 || self.parentControllerType == 2) {
    AddExpenseViewController *parent = [self.navigationController.viewControllers objectAtIndex:2];
    //call the method on the parent view controller
    if(parent != nil) 
        [parent setOdometerFieldValue:readingLabel.text];
    parent = nil;
    [self.navigationController popViewControllerAnimated:YES];
        
    } 
//    else if(self.parentControllerType == 3 || self.parentControllerType == 4) 
//    {
//        AddMileageViewController *parent = [self.navigationController.viewControllers objectAtIndex:2];
//        //call the method on the parent view controller
//        if(parent != nil) {
//            if(self.parentControllerType == 3) {
//                [parent setstartOdometeValue:readingLabel.text];
//            } else if(self.parentControllerType == 4) {
//                [parent setendOdometeValue:readingLabel.text];
//            }
//        }
//         parent = nil;
//        [self.navigationController popViewControllerAnimated:YES];
//    }
//    for (int i=0; i < readingLabel.text.length; i++) {
//        NSString *theCurChar = [readingLabel.text substringWithRange:NSMakeRange(i,1)];
//        
//        DebugLog(@"%@",theCurChar);
//    }
}

#pragma mark -
#pragma mark Picker Data Source Methods
- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView
{
	return 7;
}

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component
{
	return[self.valueTypes count];
}
#pragma mark Picker Delegate Methods

-(NSString *)pickerView:(UIPickerView *)pickerView   titleForRow:(NSInteger)row   forComponent:(NSInteger)component 
{
    if (component == ThousandComponent)
        return [NSString stringWithFormat:@"%@ %@",[self. valueTypes objectAtIndex:row], @","];
    else if (component == OnesComponent)
        return [NSString stringWithFormat:@"%@ %@",[self. valueTypes objectAtIndex:row], @"."];
    else 
        return [self. valueTypes objectAtIndex:row];
}

- (void)pickerView:(UIPickerView *)thePickerView  didSelectRow:(NSInteger)row inComponent:(NSInteger)component
{
    
	//get the values
	NSInteger row_one = [thePickerView selectedRowInComponent:0];
	NSInteger row_two = [thePickerView selectedRowInComponent:1];
	NSInteger row_three = [thePickerView selectedRowInComponent:2];
	NSInteger row_four = [thePickerView selectedRowInComponent:3];
	NSInteger row_five = [thePickerView selectedRowInComponent:4];
    NSInteger row_six = [thePickerView selectedRowInComponent:5];
    NSInteger row_seven = [thePickerView selectedRowInComponent:6];
    
    NSString *value_one = [self.valueTypes objectAtIndex:row_one];
    NSString *value_two = [self.valueTypes objectAtIndex:row_two];
    NSString *value_three = [self.valueTypes objectAtIndex:row_three];
    NSString *value_four = [self.valueTypes objectAtIndex:row_four];
    NSString *value_five = [self.valueTypes objectAtIndex:row_five];
    NSString *value_six = [self.valueTypes objectAtIndex:row_six];
    NSString *value_seven = [self.valueTypes objectAtIndex:row_seven];
    

    if([value_one isEqualToString:@"0"] && [value_two isEqualToString:@"0"] && [value_three isEqualToString:@"0"] && [value_four isEqualToString:@"0"] && [value_five isEqualToString:@"0"] && [value_six isEqualToString:@"0"] && [value_seven isEqualToString:@"0"]) 
        readingLabel.text = [NSString stringWithFormat:@"%@",value_seven];
    
    else if([value_one isEqualToString:@"0"] && [value_two isEqualToString:@"0"] && [value_three isEqualToString:@"0"] && [value_four isEqualToString:@"0"] && [value_five isEqualToString:@"0"] && [value_six isEqualToString:@"0"]) 
        readingLabel.text = [NSString stringWithFormat:@"%@.%@",value_six,value_seven];
    else if([value_one isEqualToString:@"0"] && [value_two isEqualToString:@"0"] && [value_three isEqualToString:@"0"] && [value_four isEqualToString:@"0"] && [value_five isEqualToString:@"0"]) 
        readingLabel.text = [NSString stringWithFormat:@"%@.%@",value_six,value_seven];
    else if([value_one isEqualToString:@"0"] && [value_two isEqualToString:@"0"] && [value_three isEqualToString:@"0"] && [value_four isEqualToString:@"0"]) 
        readingLabel.text = [NSString stringWithFormat:@"%@%@.%@",value_five,value_six,value_seven];
    else if([value_one isEqualToString:@"0"] && [value_two isEqualToString:@"0"] && [value_three isEqualToString:@"0"]) 
        readingLabel.text = [NSString stringWithFormat:@"%@%@%@.%@",value_four,value_five,value_six,value_seven];
    else if([value_one isEqualToString:@"0"] && [value_two isEqualToString:@"0"]) 
        readingLabel.text = [NSString stringWithFormat:@"%@,%@%@%@.%@",value_three,value_four,value_five,value_six,value_seven];
    else  if([value_one isEqualToString:@"0"])
        readingLabel.text = [NSString stringWithFormat:@"%@%@,%@%@%@.%@",value_two,value_three,value_four,value_five,value_six,value_seven];
    else 
        readingLabel.text = [NSString stringWithFormat:@"%@%@%@,%@%@%@.%@",value_one,value_two,value_three,value_four,value_five,value_six,value_seven];
    
    
    
}

- (void)viewDidUnload
{
    self.readingLabel = nil;
    self.odometerPicker = nil;
    [self setPickerMainView:nil];
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}


- (void)dealloc {
    self.setvalue = nil;
    [readingLabel release];
	[odometerPicker release];
	[valueTypes release];
    [pickerMainView release];
    [super dealloc];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

-(BOOL)shouldAutorotate
{
    return NO;
}

-(NSUInteger)supportedInterfaceOrientations
{
    return UIInterfaceOrientationMaskPortrait;
}

@end
