//
//  OdometerPickerViewController.h
//  sqllite sample
//
//  Created by Rishi Saxena on 26/04/12.
//  Copyright (c) 2012 Phonethics. All rights reserved.
//

#import <UIKit/UIKit.h>
#define DecimalComponent  6
#define OnesComponent  5
#define TensComponent  4
#define HundredComponent  3
#define ThousandComponent  2
#define TenThousandComponent  1
#define LakhComponent  0



@interface OdometerPickerViewController : UIViewController <UIPickerViewDelegate, UIPickerViewDataSource>{
    NSArray *valueTypes;
    NSString *setvalue;
    int parentControllerType;
    
}
@property (strong, nonatomic) IBOutlet UILabel *readingLabel;
@property (strong, nonatomic) IBOutlet UIPickerView *odometerPicker;
@property (nonatomic,retain)  NSArray *valueTypes;
@property (nonatomic,copy)  NSString *setvalue;
@property(nonatomic, assign) int parentControllerType;
@property (retain, nonatomic) IBOutlet UIImageView *pickerMainView;

@end
