//
//  CarUtilFourthViewController.h
//  CarUtil
//
//  Created by Rishi Saxena on 29/03/12.
//  Copyright (c) 2012 Phonethics. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <AVFoundation/AVFoundation.h>
#import <AudioToolbox/AudioToolbox.h>
#import "MWPhotoBrowser.h"

#define MAX_GRILL_COUNT 5
#define MIN_GRILL_COUNT 1
@class CarUtilGallery;
@interface CarUtilFourthViewController : UIViewController <UIScrollViewDelegate,AVAudioPlayerDelegate,MWPhotoBrowserDelegate,NSXMLParserDelegate>{
    UIScrollView *imgscrollView;
    UIButton *abtbut1;
    UIImageView *imageView2;
    UIButton *abtbut2;
    UIButton *abtbut3;
    UIImageView *imageView4;
    UIButton *abtbut5;
    NSArray *_photos;
    AVAudioPlayer *audioPlayer;
    AVAudioPlayer *audioPlayer1;
    AVAudioPlayer *audioPlayer2;
    IBOutlet UIView *view1;
    int noOfgrills;
    NSXMLParser *xmlParser;
    NSMutableData *responseAsyncData;
    int status;
    CarUtilGallery *galleryObj;
    BOOL elementFound;
}
@property (nonatomic, retain) AVAudioPlayer *audioPlayer;
@property (nonatomic, retain) AVAudioPlayer *audioPlayer1;
@property (nonatomic, retain) AVAudioPlayer *audioPlayer2;
@property (nonatomic, retain) NSArray *photos;
@property (nonatomic, copy) NSString *socialLink;
@property (nonatomic, retain) IBOutlet UIScrollView *imgscrollView;
@property (retain, nonatomic) IBOutlet  UIView *view1;
@property (retain, nonatomic) IBOutlet UIImageView *img2;
@property (retain, nonatomic) IBOutlet UIButton *but2;
@property (strong, nonatomic) NSMutableArray *galleryArray;

- (IBAction)swipeDetected:(UIGestureRecognizer *)sender ;
//@property (nonatomic, retain) IBOutlet UIImageView *abtImage;
@end
