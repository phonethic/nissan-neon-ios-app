//
//  CarBrand.m
//  CarUtil
//
//  Created by Rishi on 05/02/13.
//  Copyright (c) 2013 Phonethics. All rights reserved.
//

#import "CarBrand.h"

@implementation CarBrand
@synthesize carmodelid,carmodel;

-(id)initWithID:(int)lcarmodelid image:(NSString *)lcarmodel
{
    self = [super init];
    
    if(self) {
        self.carmodelid = lcarmodelid;
        self.carmodel = lcarmodel;
    }
	return self;
}

-(void)dealloc
{
    self.carmodel = nil;
    [super dealloc];
}
@end
