//
//  ImageViewController.h
//  CarUtil
//
//  Created by Rishi Saxena on 03/04/12.
//  Copyright (c) 2012 Phonethics. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol VideoPlayProtocol <NSObject>
@required
- (void) playVideo: (int)videoId;
@end

@interface ImageViewController : UIViewController {
    int pageNumber;
    int scrollType;
    UIImageView *imgView;
}

@property (strong, nonatomic) IBOutlet UIImageView *imgView;
@property (copy,nonatomic) NSString *imageUrl;
@property (copy,nonatomic) NSString *videoUrl;
- (id)initWithPageNumber:(int)page;
- (id)initWithImageUrl:(NSString *)limageUrl videourl:(NSString *)lvideoUrl;
- (UIView *)getView;
-(void)setScrollType:(int)ltype;
@end

