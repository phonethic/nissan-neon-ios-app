//
//  ImageGalleryViewController.m
//  CarUtil
//
//  Created by Rishi Saxena on 17/04/12.
//  Copyright (c) 2012 Phonethics. All rights reserved.
//

#import "ImageGalleryViewController.h"

@interface ImageGalleryViewController ()

@end

@implementation ImageGalleryViewController
@synthesize photos = _photos;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        self.title = NSLocalizedString(@"Gallery", @"Gallery");
        self.tabBarItem.image = [UIImage imageNamed:@"gallery"];
    }
    return self;
}
#pragma mark - MWPhotoBrowserDelegate

- (NSUInteger)numberOfPhotosInPhotoBrowser:(MWPhotoBrowser *)photoBrowser {
    return _photos.count;
}

- (MWPhoto *)photoBrowser:(MWPhotoBrowser *)photoBrowser photoAtIndex:(NSUInteger)index {
    if (index < _photos.count)
        return [_photos objectAtIndex:index];
    return nil;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    // Do any additional setup after loading the view from its nib.
    NSMutableArray *photos = [[NSMutableArray alloc] init];
    MWPhoto *photo;
    photo = [MWPhoto photoWithFilePath:[[NSBundle mainBundle] pathForResource:@"Micra0" ofType:@"jpg"]];
    photo.caption = @"";
    [photos addObject:photo];
    photo = [MWPhoto photoWithFilePath:[[NSBundle mainBundle] pathForResource:@"Micra1" ofType:@"jpg"]];
    photo.caption = @"";
    [photos addObject:photo];
    photo = [MWPhoto photoWithFilePath:[[NSBundle mainBundle] pathForResource:@"Micra2" ofType:@"jpg"]];
    photo.caption = @"";
    [photos addObject:photo];
    photo = [MWPhoto photoWithFilePath:[[NSBundle mainBundle] pathForResource:@"Micra3" ofType:@"jpg"]];
    photo.caption = @"";
    [photos addObject:photo];
    photo = [MWPhoto photoWithFilePath:[[NSBundle mainBundle] pathForResource:@"Micra4" ofType:@"jpg"]];
    photo.caption = @"";
    [photos addObject:photo];
    photo = [MWPhoto photoWithFilePath:[[NSBundle mainBundle] pathForResource:@"Micra5" ofType:@"jpg"]];
    photo.caption = @"";
    [photos addObject:photo];
    photo = [MWPhoto photoWithFilePath:[[NSBundle mainBundle] pathForResource:@"Micra6" ofType:@"jpg"]];
    photo.caption = @"";
    [photos addObject:photo];
    photo = [MWPhoto photoWithFilePath:[[NSBundle mainBundle] pathForResource:@"Micra7" ofType:@"jpg"]];
    photo.caption = @"";
    [photos addObject:photo];
    photo = [MWPhoto photoWithFilePath:[[NSBundle mainBundle] pathForResource:@"Sunny" ofType:@"tif"]];
    photo.caption = @"";
    [photos addObject:photo];
    photo = [MWPhoto photoWithFilePath:[[NSBundle mainBundle] pathForResource:@"Sunny2" ofType:@"tif"]];
    photo.caption = @"";
    [photos addObject:photo];
    photo = [MWPhoto photoWithFilePath:[[NSBundle mainBundle] pathForResource:@"Sunny3" ofType:@"tif"]];
    photo.caption = @"";
    [photos addObject:photo];
    photo = [MWPhoto photoWithFilePath:[[NSBundle mainBundle] pathForResource:@"Sunny4" ofType:@"tif"]];
    photo.caption = @"";
    [photos addObject:photo];
    photo = [MWPhoto photoWithFilePath:[[NSBundle mainBundle] pathForResource:@"Sunny5" ofType:@"tif"]];
    photo.caption = @"";
    [photos addObject:photo];
    photo = [MWPhoto photoWithFilePath:[[NSBundle mainBundle] pathForResource:@"Sunny6" ofType:@"tif"]];
    photo.caption = @"";
    [photos addObject:photo];
    photo = [MWPhoto photoWithFilePath:[[NSBundle mainBundle] pathForResource:@"Sunny7" ofType:@"tif"]];
    photo.caption = @"";
    [photos addObject:photo];
    photo = [MWPhoto photoWithFilePath:[[NSBundle mainBundle] pathForResource:@"NSOI_Portrait_image" ofType:@"jpg"]];
    photo.caption = @"";
    [photos addObject:photo];
    self.photos = photos;
    
    // Create browser
	MWPhotoBrowser *browser = [[MWPhotoBrowser alloc] initWithDelegate:self];
    browser.displayActionButton = YES;
    
    [self.navigationController pushViewController:browser animated:YES];
    self.tabBarController.hidesBottomBarWhenPushed = NO;
    // Release
	[browser release];
	[photos release];

}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

@end
