//
//  animationsTestViewController.h
//  animationsTest
//
//  Created by Rishi Saxena on 28/03/12.
//  Copyright (c) 2012 Phonethics. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MediaPlayer/MediaPlayer.h>
#import <AVFoundation/AVFoundation.h>
#import <AudioToolbox/AudioToolbox.h>

@interface animationsTestViewController : UIViewController <AVAudioPlayerDelegate>{
    UIImageView *micraView;
    UIImageView *sunnyView;
    UIImageView *xtrailView;
    UIImageView *micraBody;
    UIImageView *sunnyBody;
    UIImageView *xtrailBody;
    UIImageView *micratyre1;
    UIImageView *micratyre2;
    UIImageView *sunnytyre1;
    UIImageView *sunnytyre2;
    UIImageView *xtrailtyre1;
    UIImageView *xtrailtyre2;
    UIButton *enterButton;
    int stopAnim;
    AVAudioPlayer *audioPlayer;
        
}
@property (strong, nonatomic) MPMoviePlayerController *moviePlayer;
@property (nonatomic, retain) IBOutlet UIImageView *micraView;
@property (nonatomic, retain) IBOutlet UIImageView *sunnyView;
@property (nonatomic, retain) IBOutlet UIImageView *xtrailView;
@property (nonatomic, retain) IBOutlet UIImageView *micraBody;
@property (nonatomic, retain) IBOutlet UIImageView *sunnyBody;
@property (nonatomic, retain) IBOutlet UIImageView *xtrailBody;
@property (nonatomic, retain) IBOutlet UIImageView *micratyre1;
@property (nonatomic, retain) IBOutlet UIImageView *micratyre2;
@property (nonatomic, retain) IBOutlet UIImageView *sunnytyre1;
@property (nonatomic, retain) IBOutlet UIImageView *sunnytyre2;
@property (nonatomic, retain) IBOutlet UIImageView *xtrailtyre1;
@property (nonatomic, retain) IBOutlet UIImageView *xtrailtyre2;
@property (nonatomic, retain) IBOutlet UIButton *enterButton;
@property (nonatomic, retain) IBOutlet UIButton *enterButton1;
@property (nonatomic, retain) IBOutlet UIButton *enterButton2;
@property (nonatomic, retain) IBOutlet UIButton *resetButton;
@property (nonatomic, retain) AVAudioPlayer *audioPlayer;

- (IBAction)changeAnimation:(id)sender;
- (IBAction)changeAnimation1:(id)sender;
- (IBAction)changeAnimation2:(id)sender;
- (IBAction)reset:(id)sender;
@end
