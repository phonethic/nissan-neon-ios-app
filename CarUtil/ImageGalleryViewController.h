//
//  ImageGalleryViewController.h
//  CarUtil
//
//  Created by Rishi Saxena on 17/04/12.
//  Copyright (c) 2012 Phonethics. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MWPhotoBrowser.h"

@interface ImageGalleryViewController : UIViewController <MWPhotoBrowserDelegate>{
     NSArray *_photos;
}
@property (nonatomic, retain) NSArray *photos;
@end
