//
//  CarUtilWebViewController.m
//  CarUtil
//
//  Created by Kirti Nikam on 18/02/13.
//  Copyright (c) 2013 Phonethics. All rights reserved.
//
#import <QuartzCore/QuartzCore.h>
#import "CarUtilWebViewController.h"

@interface CarUtilWebViewController ()

@end

@implementation CarUtilWebViewController
@synthesize webView;
@synthesize loadingView;
@synthesize spinnerIndicator;
@synthesize barbackBtn;
@synthesize barfwdBtn;
@synthesize barrefreshBtn;
@synthesize webviewtoolBar;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    [Flurry logEvent:@"Nissan_Facebook_Page_Event"];
    barbackBtn.enabled      = NO;
    barfwdBtn.enabled       = NO;
    barrefreshBtn.enabled   = NO;
    loadingView.layer.cornerRadius = 10;
    NSURLRequest *requestObj = [NSURLRequest requestWithURL:[NSURL URLWithString:WEB_LINK]];
    [webView loadRequest:requestObj];
}

- (void)viewDidUnload
{
    [self setWebView:nil];
    [self setSpinnerIndicator:nil];
    [self setLoadingView:nil];
    [self setBarbackBtn:nil];
    [self setBarfwdBtn:nil];
    [self setBarrefreshBtn:nil];
    [self setWebviewtoolBar:nil];
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return YES;
}

-(BOOL)shouldAutorotate
{
    return YES;
}

-(NSUInteger)supportedInterfaceOrientations
{
    return UIInterfaceOrientationMaskAllButUpsideDown;
}

- (void)dealloc {
    [webView release];
    [spinnerIndicator release];
    [loadingView release];
    [barbackBtn release];
    [barfwdBtn release];
    [barrefreshBtn release];
    [webviewtoolBar release];
    [super dealloc];
}

#pragma mark webView Delegate methods

- (void)webViewDidStartLoad:(UIWebView *)webView
{
    barrefreshBtn.enabled   = NO;
}

- (void)webViewDidFinishLoad:(UIWebView *)webView
{
    if (self.webView.canGoBack)
    {
        barbackBtn.enabled = YES;
    }
    else
    {
        barbackBtn.enabled = NO;
    }
    if (self.webView.canGoForward)
    {
        barfwdBtn.enabled = YES;
    }
    else
    {
        barfwdBtn.enabled = NO;
    }
    barrefreshBtn.enabled = YES;
    [spinnerIndicator stopAnimating];
    [loadingView setHidden:YES];
}

- (void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error
{
    barrefreshBtn.enabled = YES;
    [spinnerIndicator stopAnimating];
    [loadingView setHidden:YES];
}
- (IBAction)webviewbackBtnPressed:(id)sender {
    [webView goBack];
}

- (IBAction)webviewfwdBtnPressed:(id)sender {
    [webView goForward];
}

- (IBAction)webviewrefreshBtnPressed:(id)sender {
    [webView reload];
}
@end
