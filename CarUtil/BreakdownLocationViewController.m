//
//  BreakdownLocationViewController.m
//  CarUtil
//
//  Created by Kirti Nikam on 06/02/13.
//  Copyright (c) 2013 Phonethics. All rights reserved.
//

#import "BreakdownLocationViewController.h"
#import <QuartzCore/QuartzCore.h>

@interface BreakdownLocationViewController ()

@end

@implementation BreakdownLocationViewController
@synthesize mapView;
@synthesize loadingView;
@synthesize proceedView;
@synthesize phonefield;
@synthesize proceedBtn;
@synthesize currentLatitude,currentLongitude;
@synthesize reverseAddress;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}
- (void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
    [locationManager stopUpdatingLocation];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    mapView.mapType = MKMapTypeStandard;
    mapView.zoomEnabled = YES;
    mapView.scrollEnabled = YES;
    mapView.delegate = self;
    mapView.showsUserLocation = YES;
    [self.view addSubview:mapView];
    
    loadingView.backgroundColor = [UIColor blackColor];
    loadingView.layer.cornerRadius = 10;
    loadingView.hidden = NO;
    [self.view bringSubviewToFront:loadingView];
    
    proceedView.layer.cornerRadius = 10;
    
    locationManager = [[CLLocationManager alloc] init];
    locationManager.delegate = self;
    locationManager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters;
    locationManager.distanceFilter = kCLDistanceFilterNone;
    [locationManager startUpdatingLocation];
    [self performSelector:@selector(stopUpdatingCoreLocation:) withObject:@"Timed Out" afterDelay:30];
}
- (void) locationManager:(CLLocationManager *) manager didFailWithError:(NSError *) error
{
    [locationManager stopUpdatingLocation];
}

- (void)stopUpdatingCoreLocation:(NSString *)state
{
    [locationManager stopUpdatingLocation];
    [self saveCurrentocation:[locationManager location]];
}

-(void)saveCurrentocation:(CLLocation *)lLocation
{
    // If it's not possible to get a location, then return.
	if (!lLocation) {
        UIAlertView *errorAlert = [[[UIAlertView alloc] initWithTitle:ALERT_TITLE
                                                             message:@"Failed to get your current location. Please check your internet connection and try again."
                                                            delegate:nil
                                                   cancelButtonTitle:@"OK"
                                                   otherButtonTitles:nil] autorelease];
        [errorAlert show];
        [loadingView setHidden:YES];
		return;
	}
    // Configure the new event with information from the location.
    [self.mapView setRegion:MKCoordinateRegionMake([lLocation coordinate], MKCoordinateSpanMake(0.2, 0.2))];
    [loadingView setHidden:YES];
    currentLatitude  = lLocation.coordinate.latitude;
    currentLongitude = lLocation.coordinate.longitude;
    DebugLog(@"%f --- %f",currentLatitude,currentLongitude);
    [self getReverseAddress];
    [self showAlertMessage];
}

-(void)getReverseAddress
{
    DebugLog(@"getReverseAddress");
	NSURLRequest *urlRequest = [NSURLRequest requestWithURL:[NSURL URLWithString:REVERSE_API(currentLatitude,currentLongitude)] cachePolicy:YES timeoutInterval:20.0];
	NSURLConnection *theConnection = [[NSURLConnection alloc] initWithRequest:urlRequest delegate:self startImmediately:YES];
    [theConnection autorelease];
}

-(void)showAlertMessage
{
    proceedView.hidden = FALSE;
    [phonefield becomeFirstResponder];
    [self.view bringSubviewToFront:proceedView];
}


#pragma mark textField Delegate Methods

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    return !([textField.text length]>= 13 && [string length] > range.length);
}

#pragma mark mailComposeController Delegate Methods

-(void)mailComposeController:(MFMailComposeViewController *)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError *)error{
     if (result == MFMailComposeResultSent)
     {
         [Flurry logEvent:@"Breakdown_Location_Event"];
     }
    [controller dismissModalViewControllerAnimated:YES];
     proceedView.hidden = TRUE;
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark Connection Delegate Methods

-(void) connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *) response
{

}

-(void) connection:(NSURLConnection *)connection didReceiveData:(NSData *) data
{
    if(webData==nil)
	{
		webData = [[NSMutableData alloc] initWithLength:0];
	}
    
	[webData appendData:data];
}

-(void) connection:(NSURLConnection *)connection didFailWithError:(NSError *) error
{
    DebugLog(@"didFailWithError error %@",error);
    [loadingView setHidden:YES];
}

-(void) connectionDidFinishLoading:(NSURLConnection *) connection
{
    DebugLog(@"connectionDidFinishLoading");
    if (webData)
    {
        NSString *result = [[NSString alloc] initWithData:webData encoding:NSASCIIStringEncoding];
		DebugLog(@"\n result:%@\n\n", result);
        
        NSString *xmlDataFromChannelSchemes = [[NSString alloc] initWithString:result];
		NSData *xmlDataInNSData = [xmlDataFromChannelSchemes dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];
		xmlParser = [[NSXMLParser alloc] initWithData:xmlDataInNSData];
		[xmlParser setDelegate:self];
		[xmlParser parse];
        [xmlParser release];
		[xmlDataFromChannelSchemes release];
		[result release];
		[webData release];
		webData = nil;
    }
    else
    {
        DebugLog(@"webdata = %@",webData);
    }
}

#pragma mark XML Parser Delegate Methods

-(void)parser:(NSXMLParser *)parser didStartElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName attributes:(NSDictionary *)attributeDict
{
    if ([elementName isEqualToString:@"GeocodeResponse"])
    {
    }
    else if ([elementName isEqualToString:@"result"])
    {
    }
    else if ([elementName isEqualToString:@"formatted_address"])
    {
      //  reverseAddress = [[NSString alloc] init];
        elementFound = YES;
    }
}

-(void)parser:(NSXMLParser *)parser foundCharacters:(NSString *)string{
    
    if (elementFound)
    {
        reverseAddress = [[NSString alloc] initWithString:string];
        DebugLog(@"-- %@ ---",reverseAddress);
    }
    elementFound = FALSE;
}

-(void)parser:(NSXMLParser *)parser didEndElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName
{
}

-(void)parser:(NSXMLParser *)parser validationErrorOccurred:(NSError *)validationError{
}

-(void)parserDidEndDocument:(NSXMLParser *)parser{
    
}

- (void)viewDidUnload
{
    [self setMapView:nil];
    [self setReverseAddress:nil];
    [self setLoadingView:nil];
    [self setProceedView:nil];
    [self setPhonefield:nil];
    [self setProceedBtn:nil];
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

-(BOOL)shouldAutorotate
{
    return NO;
}

-(NSUInteger)supportedInterfaceOrientations
{
    return UIInterfaceOrientationMaskPortrait;
}

- (void)dealloc {
    [mapView release];
    [loadingView release];
    [reverseAddress release];
    reverseAddress = nil;
    [proceedView release];
    [phonefield release];
    [proceedBtn release];
    [super dealloc];
}
- (IBAction)proceedBtnPressed:(id)sender {
    @try{
        MFMailComposeViewController *picker=[[[MFMailComposeViewController alloc]init] autorelease];
        picker.mailComposeDelegate=self;
        NSString *msgBody;
        if (reverseAddress == nil)
        {
            msgBody = [NSString stringWithFormat:@"Address URL:  %@",MAP_URL(currentLatitude,currentLongitude)];
        }
        else
        {
            msgBody=[NSString stringWithFormat:@"Address:  %@\n\nAddress URL:  %@",reverseAddress,MAP_URL(currentLatitude,currentLongitude)];
        }
        picker.navigationBar.tintColor = [UIColor blackColor];
        [picker setToRecipients:[NSArray arrayWithObject:BREAKDOWN_EMAILID]];
        [picker setSubject:[NSString stringWithFormat:@"Breakdown %@",phonefield.text]];
        [picker setMessageBody:msgBody isHTML:NO];
        [self presentModalViewController:picker animated:YES];
    }
    @catch (NSException *exception)
    {
        //DebugLog(@"Exception %@",exception.reason);
    }
}
@end
