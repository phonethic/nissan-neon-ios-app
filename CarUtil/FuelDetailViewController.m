//
//  AddMFuelDetailViewController.m
//  CarUtil
//
//  Created by Rishi Saxena on 18/05/12.
//  Copyright (c) 2012 Phonethics. All rights reserved.
//

#import "FuelDetailViewController.h"
#import "FuelDetail.h"
//#import "AddMileageViewController.h"
#import "CarUtilAppDelegate.h"
#import "AddEcditFuelViewController.h"

@interface FuelDetailViewController ()

@end

@implementation FuelDetailViewController
@synthesize fuelTable;
@synthesize calcMileageBtn;
@synthesize mileageBoxView;
@synthesize mileageBoxImageView;
@synthesize mileageBoxlbl;
@synthesize dayslbl;
@synthesize odometerSumlbl;
@synthesize fuelSumlbl;
@synthesize costSumlbl;
@synthesize fuelArray;
@synthesize footerView;
@synthesize headerView;
@synthesize dbowID;
@synthesize totalfooterView;
@synthesize cardId;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewWillAppear:(BOOL)animated
{
    if (isSessionEnded) {
        DebugLog(@"session ended");
        [self addTotal];
        [self.totalfooterView setHidden:FALSE];
        self.fuelTable.frame = CGRectMake(self.fuelTable.frame.origin.x, self.fuelTable.frame.origin.y, self.fuelTable.frame.size.width, 190);
    } else {
        [self.totalfooterView setHidden:TRUE];
        self.fuelTable.frame = CGRectMake(self.fuelTable.frame.origin.x, self.fuelTable.frame.origin.y, self.fuelTable.frame.size.width, 253);
    }
    [super viewWillAppear:animated];
}
#pragma mark Table view methods
//- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
//{
//    return 59;
//}
//
//- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section   // custom view for header. will be adjusted to default or specified header height
//{
//    
//    if(headerView == nil) {
//        //allocate the view if it doesn't exist yet
//        headerView  = [[UIView alloc] initWithFrame:CGRectMake(0,0.0f,320,59)];
//        //headerView.backgroundColor = [UIColor lightGrayColor];
//        
//        UIImage *image = [UIImage imageNamed:@"heading_2.png"];
//        
//        UIImageView *tempImg = [[[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 300,50)] autorelease];
//        [tempImg setImage:image];
//        
//        [headerView insertSubview:tempImg atIndex:0];  
//        
//    }
//    
//    //return the view for the footer
//    return headerView;
//}

// specify the height of your footer section
//- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
//    //differ between your sections or if you
//    //have only on section return a static value
//    return 59;
//}

//// custom view for footer. will be adjusted to default or specified footer height
//// Notice: this will work only for one section within the table view
//- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section {
//    
//    if(footerView == nil) {
//        //allocate the view if it doesn't exist yet
//        footerView  = [[UIView alloc] initWithFrame:CGRectMake(0.0f,0.0f,320,59)];
//        footerView.backgroundColor = [UIColor clearColor];
//       
//        UILabel* dayslbl  = [[[UILabel alloc] initWithFrame:CGRectMake(35,8, 70,40)] autorelease];
//        dayslbl.backgroundColor   = [UIColor clearColor];
//       // dayslbl.text      =  @"100";
//        dayslbl.baselineAdjustment= UIBaselineAdjustmentAlignCenters;
//        dayslbl.lineBreakMode =  UILineBreakModeTailTruncation;
//        dayslbl.textAlignment = UITextAlignmentCenter;
//        dayslbl.font      = [UIFont systemFontOfSize:20];
//        dayslbl.textColor     = [UIColor whiteColor];
//        dayslbl.numberOfLines = 0;
//
//        UILabel* odometerSumlbl  = [[[UILabel alloc] initWithFrame:CGRectMake(dayslbl.frame.origin.x+dayslbl.frame.size.width+5,8, 70,40)] autorelease];
//        odometerSumlbl.backgroundColor   = [UIColor clearColor];
//        //odometerSumlbl.text      =  @"100";
//        odometerSumlbl.baselineAdjustment= UIBaselineAdjustmentAlignCenters;
//        odometerSumlbl.lineBreakMode =  UILineBreakModeTailTruncation;
//        odometerSumlbl.textAlignment = UITextAlignmentCenter;
//        odometerSumlbl.font      = [UIFont systemFontOfSize:20];
//        odometerSumlbl.textColor     = [UIColor whiteColor];
//        odometerSumlbl.numberOfLines = 0;
//
//        UILabel* fuellbl  = [[[UILabel alloc] initWithFrame:CGRectMake(odometerSumlbl.frame.origin.x+odometerSumlbl.frame.size.width+10, 8, 50, 40)] autorelease];
//        fuellbl.backgroundColor   = [UIColor clearColor];
//        //fuellbl.text      =  @"100";
//        fuellbl.baselineAdjustment= UIBaselineAdjustmentAlignCenters;
//        fuellbl.lineBreakMode =  UILineBreakModeTailTruncation;
//        fuellbl.textAlignment = UITextAlignmentCenter;
//        fuellbl.font      = [UIFont systemFontOfSize:20];
//        fuellbl.textColor     = [UIColor whiteColor];
//        fuellbl.numberOfLines = 0;
//
//        UILabel* costlbl  = [[[UILabel alloc] initWithFrame:CGRectMake(fuellbl.frame.origin.x+fuellbl.frame.size.width+5,8, 50, 40)] autorelease];
//        costlbl.backgroundColor   = [UIColor clearColor];
//       // costlbl.text      =  @"100";
//        costlbl.baselineAdjustment= UIBaselineAdjustmentAlignCenters;
//        costlbl.lineBreakMode =  UILineBreakModeTailTruncation;
//        costlbl.textAlignment = UITextAlignmentCenter;
//        costlbl.font      = [UIFont systemFontOfSize:20];
//        costlbl.textColor     = [UIColor whiteColor];
//        costlbl.numberOfLines = 0;
//
//        
//        UIImage *image = [UIImage imageNamed:@"total_footer.png"];
//        UIImageView *tempImg = [[[UIImageView alloc] initWithFrame:CGRectMake(0, 0,300, 59)] autorelease];
//        [tempImg setImage:image];
//        //add the button to the view
//        [footerView insertSubview:tempImg atIndex:0];
//        [footerView insertSubview:dayslbl atIndex:1];
//        [footerView insertSubview:odometerSumlbl atIndex:2];
//        [footerView insertSubview:fuellbl atIndex:3];
//        [footerView insertSubview:costlbl atIndex:4];
//
//    }
//    
//    //return the view for the footer
//    return footerView;
//}

//- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
//    DebugLog(@"willDisplayCell");
//    if (gotFull)
//    {
//        //cell.backgroundColor = [UIColor redColor];
//        cell.frame = CGRectMake(0,0,300,500.0f);
//    }
//    else
//    {
//        cell.frame = CGRectMake(0,0,300.0f,50.0f);
//    }
//}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    DebugLog(@"heightForRowAtIndexPath");
    FuelDetail *fuelObj = (FuelDetail *)[fuelArray objectAtIndex:indexPath.row];
     if (!fuelObj.isPartial && indexPath.row !=0 && fuelObj.flagType != END) {
         return 110;
     } else {
         return 50;
     }
//    if (gotFull)
//    {
//        return 200;
//    }
//    else
//    {
//        return 50;
//    }
}

// Customize the number of rows in the table view.
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    DebugLog(@"count = %d",[fuelArray count]);
    return [fuelArray count];
}

// Customize the appearance of table view cells.
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *CellIdentifier1 = @"Cell1";
    static NSString *CellIdentifier2 = @"Cell2";
    UIImageView *flagimageView;
    UILabel *lblDate;
    UILabel *lblOdometerReading;
    UILabel *lblfuel;
    UILabel *lblCost;
    
    //    // Set up the cell
	FuelDetail *fuelObj = (FuelDetail *)[fuelArray objectAtIndex:indexPath.row];
    //cell.textLabel.text = expenseObj.type;
    DebugLog(@"row  = %d",indexPath.row);
    
    if (!fuelObj.isPartial && indexPath.row !=0 && fuelObj.flagType != END && indexPath != nil) {
        double average = [self calculateAvg:indexPath.row odometerR:fuelObj.odometerReading fuel:fuelObj.fuel];
        gotFull = YES;
        DebugLog(@"average %f",average);
        
        UIImageView *interimimageView;
        UILabel *lblinterim;
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier2];
        if (cell == nil) {
            cell = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier2] autorelease];
            interimimageView = [[UIImageView alloc] initWithFrame:CGRectMake(cell.frame.origin.x+36 , cell.frame.origin.y + 5, 193, 46)];
            interimimageView.image = [UIImage imageNamed:@"interim_mileage.png"];
            interimimageView.contentMode = UIViewContentModeScaleToFill;
            interimimageView.backgroundColor =  [UIColor clearColor];
            [cell.contentView addSubview:interimimageView];
            [interimimageView release];
            
            //Initialize Label with tag 2.(Fuel Label)
            lblinterim = [[UILabel alloc] initWithFrame:CGRectMake(cell.frame.origin.x+120, cell.frame.origin.y+5, 50, 30)];
            lblinterim.tag = 6;
            lblinterim.text = @"0";
            lblinterim.font = [UIFont boldSystemFontOfSize:16];
            lblinterim.textColor = [UIColor whiteColor];
            lblinterim .lineBreakMode =  UILineBreakModeTailTruncation;
            lblinterim.textAlignment = UITextAlignmentCenter;
            lblinterim.backgroundColor =  [UIColor clearColor];
            [cell.contentView addSubview:lblinterim];
            [lblinterim release];
            
            //Initialize Image View with tag 1.(Thumbnail Image)
            flagimageView = [[UIImageView alloc] initWithFrame:CGRectMake(cell.frame.origin.x+10 , cell.frame.origin.y + 70, 12, 26)];
            flagimageView.tag = 1;
            flagimageView.contentMode = UIViewContentModeScaleToFill;
            flagimageView.backgroundColor =  [UIColor clearColor];
            [cell.contentView addSubview:flagimageView];
            [flagimageView release];
            
            //Initialize Label with tag 1.(Date Label)
            lblDate = [[UILabel alloc] initWithFrame:CGRectMake(cell.frame.origin.x+30, cell.frame.origin.y + 70, 80, cell.frame.size.height - 20)];
            lblDate.tag = 2;
            lblDate.text = @"0";
            lblDate.font = [UIFont systemFontOfSize:13];
            lblDate.textAlignment = UITextAlignmentCenter;
            lblDate.textColor = [UIColor whiteColor];
            lblDate.backgroundColor =  [UIColor clearColor];
            [cell.contentView addSubview:lblDate];
            [lblDate release];
            
            //Initialize Label with tag 2.(Fuel Label)
            lblOdometerReading  = [[UILabel alloc] initWithFrame:CGRectMake(lblDate.frame.origin.x + lblDate.frame.size.width + 2, cell.frame.origin.y + 70, 75, cell.frame.size.height - 20)];
            lblOdometerReading.tag = 3;
            lblOdometerReading.text = @"0";
            lblOdometerReading.font = [UIFont systemFontOfSize:16];
            lblOdometerReading.textColor = [UIColor whiteColor];
            lblOdometerReading .lineBreakMode =  UILineBreakModeTailTruncation;
            lblOdometerReading.textAlignment = UITextAlignmentCenter;
            lblOdometerReading.backgroundColor =  [UIColor clearColor];
            [cell.contentView addSubview:lblOdometerReading];
            [lblOdometerReading release];
            
            //Initialize Label with tag 2.(Fuel Label)
            lblfuel = [[UILabel alloc] initWithFrame:CGRectMake(lblOdometerReading.frame.origin.x + lblOdometerReading.frame.size.width + 2, cell.frame.origin.y + 70, 58, cell.frame.size.height - 20)];
            lblfuel.tag = 4;
            lblfuel.text = @"0";
            lblfuel.font = [UIFont systemFontOfSize:16];
            lblfuel.textColor = [UIColor whiteColor];
            lblfuel .lineBreakMode =  UILineBreakModeTailTruncation;
            lblfuel.textAlignment = UITextAlignmentCenter;
            lblfuel.backgroundColor =  [UIColor clearColor];
            [cell.contentView addSubview:lblfuel];
            [lblfuel release];
            
            //Initialize Label with tag 3.(Cost Label)
            lblCost = [[UILabel alloc] initWithFrame:CGRectMake(lblfuel.frame.origin.x + lblfuel.frame.size.width + 2, cell.frame.origin.y + 70, 58, cell.frame.size.height - 20)];
            lblCost.tag = 5;
            lblCost.text = @"0";
            lblCost.font = [UIFont systemFontOfSize:16];
            lblCost.textColor = [UIColor whiteColor];
            lblCost.textAlignment = UITextAlignmentCenter;
            lblCost .lineBreakMode =  UILineBreakModeTailTruncation;
            lblCost.backgroundColor =  [UIColor clearColor];
            [cell.contentView addSubview:lblCost];
            [lblCost release];

        }
        
        flagimageView = (UIImageView *)[cell viewWithTag:1];
        
        if (fuelObj.flagType == INTERMEDIATE)
            flagimageView.image = [UIImage imageNamed:@"milestone_small.png"];
        else
            flagimageView.image = [UIImage imageNamed:@"flag.png"];
        
        NSDate *pickerDate = [NSDate dateWithTimeIntervalSinceReferenceDate:fuelObj.date];
        NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
        [dateFormat setDateFormat:@"dd MMM yyyy"];
        NSString *dateString = [dateFormat stringFromDate:pickerDate];
        [dateFormat release];
        
        lblDate = (UILabel *)[cell viewWithTag:2];
        lblDate.text = dateString;
        
        lblOdometerReading = (UILabel *)[cell viewWithTag:3];
        lblOdometerReading.text = [NSString stringWithFormat:@"%.1f",fuelObj.odometerReading];
        
        if (fuelObj.flagType == END)
        {
            lblfuel = (UILabel *)[cell viewWithTag:4];
            lblfuel.text = @"";
            
            lblCost = (UILabel *)[cell viewWithTag:5];
            lblCost.text = @"";
            

        }
        else
        {
            lblfuel = (UILabel *)[cell viewWithTag:4];
            lblfuel.text = [NSString stringWithFormat:@"%.1f",fuelObj.fuel];
            
            lblCost = (UILabel *)[cell viewWithTag:5];
            lblCost.text = [NSString stringWithFormat:@"%.1f",fuelObj.cost];
        }
        
        lblinterim = (UILabel *)[cell viewWithTag:6];
        lblinterim.text = [NSString stringWithFormat:@"%.2f",average];
        
        cell.accessoryType = UITableViewCellAccessoryNone;
        cell.backgroundColor = [UIColor clearColor];
        UIImageView *tempImg = [[UIImageView alloc] initWithFrame:CGRectMake(0, 2, 300, 100)];
        [tempImg setImage:[UIImage imageNamed:@"interim_mileage_big.png"]];
        cell.backgroundView = tempImg;
        [tempImg release];

        return cell;

    }
    else
    {
        DebugLog(@"got partial");
    

        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier1];
        if (cell == nil) {
            cell = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier1] autorelease];
            
            //Initialize Image View with tag 1.(Thumbnail Image)
            flagimageView = [[UIImageView alloc] initWithFrame:CGRectMake(cell.frame.origin.x+10 , cell.frame.origin.y + 10, 12, 26)];
            flagimageView.tag = 1;
            flagimageView.contentMode = UIViewContentModeScaleToFill;
            flagimageView.backgroundColor =  [UIColor clearColor];
            [cell.contentView addSubview:flagimageView];
            [flagimageView release];

            //Initialize Label with tag 1.(Date Label)
            lblDate = [[UILabel alloc] initWithFrame:CGRectMake(cell.frame.origin.x+30, cell.frame.origin.y + 12, 80, cell.frame.size.height - 20)];
            lblDate.tag = 2;
            lblDate.text = @"0";
            lblDate.font = [UIFont systemFontOfSize:13];
            lblDate.textAlignment = UITextAlignmentCenter;
            lblDate.textColor = [UIColor whiteColor];
            lblDate.backgroundColor =  [UIColor clearColor];
            [cell.contentView addSubview:lblDate];
            [lblDate release];
            
            //Initialize Label with tag 2.(Fuel Label)
            lblOdometerReading  = [[UILabel alloc] initWithFrame:CGRectMake(lblDate.frame.origin.x + lblDate.frame.size.width + 2, cell.frame.origin.y + 12, 75, cell.frame.size.height - 20)];
            lblOdometerReading.tag = 3;
            lblOdometerReading.text = @"0";
            lblOdometerReading.font = [UIFont systemFontOfSize:16];
            lblOdometerReading.textColor = [UIColor whiteColor];
            lblOdometerReading .lineBreakMode =  UILineBreakModeTailTruncation;
            lblOdometerReading.textAlignment = UITextAlignmentCenter;
            lblOdometerReading.backgroundColor =  [UIColor clearColor];
            [cell.contentView addSubview:lblOdometerReading];
            [lblOdometerReading release];

            //Initialize Label with tag 2.(Fuel Label)
            lblfuel = [[UILabel alloc] initWithFrame:CGRectMake(lblOdometerReading.frame.origin.x + lblOdometerReading.frame.size.width + 2, cell.frame.origin.y + 12, 58, cell.frame.size.height - 20)];
            lblfuel.tag = 4;
            lblfuel.text = @"0";
            lblfuel.font = [UIFont systemFontOfSize:16];
            lblfuel.textColor = [UIColor whiteColor];
            lblfuel .lineBreakMode =  UILineBreakModeTailTruncation;
            lblfuel.textAlignment = UITextAlignmentCenter;
            lblfuel.backgroundColor =  [UIColor clearColor];
            [cell.contentView addSubview:lblfuel];
            [lblfuel release];
            
            //Initialize Label with tag 3.(Cost Label)
            lblCost = [[UILabel alloc] initWithFrame:CGRectMake(lblfuel.frame.origin.x + lblfuel.frame.size.width + 2, cell.frame.origin.y + 12, 58, cell.frame.size.height - 20)];
            lblCost.tag = 5;
            lblCost.text = @"0";
            lblCost.font = [UIFont systemFontOfSize:16];
            lblCost.textColor = [UIColor whiteColor];
            lblCost.textAlignment = UITextAlignmentCenter;
            lblCost .lineBreakMode =  UILineBreakModeTailTruncation;
            lblCost.backgroundColor =  [UIColor clearColor];
            [cell.contentView addSubview:lblCost];
            [lblCost release];
        }
        flagimageView = (UIImageView *)[cell viewWithTag:1];
        
        if (fuelObj.flagType == INTERMEDIATE)
            flagimageView.image = [UIImage imageNamed:@"milestone_small.png"];
        else
            flagimageView.image = [UIImage imageNamed:@"flag.png"];
        
        NSDate *pickerDate = [NSDate dateWithTimeIntervalSinceReferenceDate:fuelObj.date];
        NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
        [dateFormat setDateFormat:@"dd MMM yyyy"];
        NSString *dateString = [dateFormat stringFromDate:pickerDate];
        [dateFormat release];
        
        lblDate = (UILabel *)[cell viewWithTag:2];
        lblDate.text = dateString;
        
        lblOdometerReading = (UILabel *)[cell viewWithTag:3];
        lblOdometerReading.text = [NSString stringWithFormat:@"%.1f",fuelObj.odometerReading];
        
        if (fuelObj.flagType == END)
        {
            lblfuel = (UILabel *)[cell viewWithTag:4];
            lblfuel.text = @"";
            
            lblCost = (UILabel *)[cell viewWithTag:5];
            lblCost.text = @"";
            
        }
        else
        {
            lblfuel = (UILabel *)[cell viewWithTag:4];
            lblfuel.text = [NSString stringWithFormat:@"%.1f",fuelObj.fuel];
            
            lblCost = (UILabel *)[cell viewWithTag:5];
            lblCost.text = [NSString stringWithFormat:@"%.1f",fuelObj.cost];
        }
        cell.accessoryType = UITableViewCellAccessoryNone;
        cell.backgroundColor = [UIColor clearColor];
        UIImageView *tempImg = [[UIImageView alloc] initWithFrame:CGRectMake(0, 2, 300, 42)];
        [tempImg setImage:[UIImage imageNamed:@"field2.png"]];
        cell.backgroundView = tempImg;
        [tempImg release];

        return cell;
    }

    return nil;
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    //Expense *expenseObj = (Expense *)[expenseArray objectAtIndex:indexPath.row];
    //[self editData:expenseObj.expenseid rowID:indexPath.row];
    //[expenseArray removeObject:expenseObj];
    [fuelTable deselectRowAtIndexPath:[fuelTable indexPathForSelectedRow] animated:NO];
    
}

//- (void)tableView:(UITableView *)tv commitEditingStyle:(UITableViewCellEditingStyle)editingStyle 
//forRowAtIndexPath:(NSIndexPath *)indexPath {
//	
//	if(editingStyle == UITableViewCellEditingStyleDelete) {
//		
//		//Get the object to delete from the array.
//        FuelDetail *fuelObj = (FuelDetail *)[fuelArray objectAtIndex:indexPath.row];
//        [self deleteMileageBreakdata:fuelObj.fueldetailid];
//		[fuelArray removeObject:fuelObj];
//		
//       //Delete the object from the table.
//		[self.fuelTable deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationMiddle];
//	}
//}

-(void)deleteMileageBreakdata:(NSInteger)lfuelid
{
    sqlite3_stmt    *statement;
    
    const char *dbpath = [[(CarUtilAppDelegate *)[[UIApplication sharedApplication] delegate] databasePath] UTF8String];
    
    if (sqlite3_open(dbpath, &expenseDB) == SQLITE_OK)
    {
        NSString *deleteSQL = [NSString stringWithFormat:@"DELETE FROM MILEAGE_BREAKS WHERE id=\"%d\"", lfuelid];
        const char *delete_stmt = [deleteSQL UTF8String];
        sqlite3_prepare_v2(expenseDB, delete_stmt, -1, &statement, NULL);
        if (sqlite3_step(statement) == SQLITE_DONE)
        {
            //[self showMessage:@"Contact deleted"];
            //[self clearData:nil];
        } else {
            //[self showMessage:@"Failed to delete contact"];
        }
        sqlite3_finalize(statement);
    }         
    sqlite3_close(expenseDB);
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    DebugLog(@"fuelDetail : viewLoad");
    isSessionEnded = NO;
    gotFull = NO;
    fuelTable.separatorStyle = UITableViewCellSeparatorStyleNone;
    UIBarButtonItem *typesaveButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemAdd target:self action:@selector(AddFuel)];
    self.navigationItem.rightBarButtonItem = typesaveButton;
    [typesaveButton release];
    [self readMileageBreakFromDatabase];
    [fuelTable reloadData];
}
- (void) AddFuel
{
    AddEcditFuelViewController *addfueldetailsController = [[AddEcditFuelViewController alloc] initWithNibName:@"AddEcditFuelViewController" bundle:nil] ;
    addfueldetailsController.hidesBottomBarWhenPushed = NO;
    if (fuelArray.count == 0)
    {
        addfueldetailsController.headerImageType = START;
    } else {
        addfueldetailsController.headerImageType = INTERMEDIATE;
        FuelDetail *fuelLastObj = (FuelDetail *)[fuelArray objectAtIndex:fuelArray.count-1];
        addfueldetailsController.minodometerValue = fuelLastObj.odometerReading;
    }
    addfueldetailsController.carId = self.cardId;
    addfueldetailsController.dbowID = self.dbowID;
    [self.navigationController pushViewController:addfueldetailsController animated:YES];
    [addfueldetailsController release];
}
-(void) readMileageBreakFromDatabase {
    
    // Init the expense Array
    if(fuelArray == nil)
    {
        DebugLog(@"new fuel array created");
        fuelArray = [[NSMutableArray alloc] init];
	} else {
        DebugLog(@"old fuel array used");
        [fuelArray removeAllObjects];
    }
//MILEAGE_BREAKS(ID INTEGER PRIMARY KEY AUTOINCREMENT, MILEAGEID INTEGER,ODOMETER_READING REAL, DATE REAL, FUEL REAL,ISPARTIAL INTEGER, COST REAL,FLAGTYPE INTEGER, CARID INTEGER)
    
//Open the database from the users filessytem
	if(sqlite3_open([[NEONAppDelegate databasePath] UTF8String], &expenseDB) == SQLITE_OK)
    {
        //NSString *findSQL = [NSString stringWithFormat:@"select ID,MILEAGEID,ODOMETER_READING,DATE,FUEL,ISPARTIAL,COST,FLAGTYPE from MILEAGE_BREAKS where MILEAGEID=\"%d\"", self.dbowID];
        NSString *findSQL = [NSString stringWithFormat:@"select * from MILEAGE_BREAKS where MILEAGEID=\"%d\"", self.dbowID];
        const char *sqlStatement = [findSQL UTF8String];
		sqlite3_stmt *compiledStatement;
		if(sqlite3_prepare_v2(expenseDB, sqlStatement, -1, &compiledStatement, NULL) == SQLITE_OK) {
			
            while(sqlite3_step(compiledStatement) == SQLITE_ROW)
            {
				// Read the data from the result row
                NSInteger primaryKey = sqlite3_column_int(compiledStatement, 0);
				int mileageIDCol = sqlite3_column_int(compiledStatement, 1);
                double odometerReadingCol = sqlite3_column_double(compiledStatement, 2);
                double dateCol = sqlite3_column_double(compiledStatement, 3);
				double fuelCol = sqlite3_column_double(compiledStatement, 4);
                int isPartialCol = sqlite3_column_int(compiledStatement, 5);
                double costCol = sqlite3_column_double(compiledStatement, 6);
                double flagTypeCol = sqlite3_column_double(compiledStatement, 7);

                DebugLog(@"primaryKey %d -mileageIDCol %d -odometerReadingCol %f -dateCol %f -fuelCol %f -isPartialCol %d -costCol %f ",primaryKey,mileageIDCol,odometerReadingCol, dateCol, fuelCol,isPartialCol, costCol);
                
                if (flagTypeCol == END)
                {
                    isSessionEnded = YES;
                    self.navigationItem.rightBarButtonItem = nil;
                }
                
			//	FuelDetail *fuelObj = [[FuelDetail alloc] initWithID:primaryKey milegaeID:mileageIDCol date:dateCol fuel:fuelCol cost:costCol];
                
                FuelDetail *fuelObj = [[FuelDetail alloc] initWithID:primaryKey milegaeID:mileageIDCol date:dateCol odometerReading:odometerReadingCol fuel:fuelCol isPartial:isPartialCol cost:costCol flagType:flagTypeCol];
				[fuelArray addObject:fuelObj];
				[fuelObj release];
			}
		}
		// Release the compiled statement from memory
		sqlite3_finalize(compiledStatement);
	}
	sqlite3_close(expenseDB);
    DebugLog(@"fuelArray %@",fuelArray);
}

- (void)viewDidUnload
{
    self.headerView = nil;
    self.footerView = nil;
    
    [self setFuelTable:nil];
    [self setCalcMileageBtn:nil];
    [self setMileageBoxImageView:nil];
    [self setMileageBoxlbl:nil];
    [self setMileageBoxView:nil];
    [self setDayslbl:nil];
    [self setOdometerSumlbl:nil];
    [self setFuelSumlbl:nil];
    [self setCostSumlbl:nil];
    [self setTotalfooterView:nil];
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

-(BOOL)shouldAutorotate
{
    return NO;
}

-(NSUInteger)supportedInterfaceOrientations
{
    return UIInterfaceOrientationMaskPortrait;
}

- (void)dealloc {
    if(headerView != nil)
        [headerView release];
    if(footerView != nil)
        [footerView release];
    if(fuelArray != nil)
        [fuelArray release];
    [fuelTable release];
    [calcMileageBtn release];
    [mileageBoxImageView release];
    [mileageBoxlbl release];
    [mileageBoxView release];
    [dayslbl release];
    [odometerSumlbl release];
    [fuelSumlbl release];
    [costSumlbl release];
    [totalfooterView release];
    [super dealloc];
}
- (IBAction)calcMileageBtnPressed:(id)sender
{
    DebugLog(@"calcMileageBtnPressed carid %d ",self.cardId);
    AddEcditFuelViewController *addfueldetailsController = [[AddEcditFuelViewController alloc] initWithNibName:@"AddEcditFuelViewController" bundle:nil] ;
    addfueldetailsController.hidesBottomBarWhenPushed = NO;
    addfueldetailsController.headerImageType = END;
    FuelDetail *fuelLastObj = (FuelDetail *)[fuelArray objectAtIndex:fuelArray.count-1];
    addfueldetailsController.minodometerValue = fuelLastObj.odometerReading;
    addfueldetailsController.carId = self.cardId;
    addfueldetailsController.dbowID = self.dbowID;
    [self.navigationController pushViewController:addfueldetailsController animated:YES];
    [addfueldetailsController release];
}
-(void)reloadFuelDetails
{
    [self readMileageBreakFromDatabase];
    DebugLog(@"Done reading from db now reloading table");
    [fuelTable reloadData];
    if (isSessionEnded)
    {
        DebugLog(@"session is ended ...updating mileage table");
        [self updateAvgMileageReading];
    }
}
-(double)calculateAvg:(int) currentIndex odometerR:(double)lodometerR fuel:(double)lfuel
{
    DebugLog(@"calculating avg ===> with lodometerR %f fuel %f",lodometerR,lfuel);
    double avg = 0;
    double sum = lfuel;
    FuelDetail *obj = nil;
    for (int index = currentIndex-1; index >= 0; index--)
    {
        obj = (FuelDetail *)[fuelArray objectAtIndex:index];
        if (obj.isPartial)
        {
            DebugLog(@"Got partial ===> with lodometerR %f fuel %f",obj.odometerReading,obj.fuel);
            sum += obj.fuel;
            if (index == 0 && index != currentIndex)
            {
                double diff = lodometerR - obj.odometerReading;
                DebugLog(@"diff %f sum %f",diff,sum);
                avg = [self calcAvg:diff sum:sum];
                break;
            }
        }
        else
        {
            DebugLog(@"Got full ===> with lodometerR %f fuel %f",obj.odometerReading,obj.fuel);
            double diff = lodometerR - obj.odometerReading;
            DebugLog(@"diff %f sum %f",diff,sum);
            avg = [self calcAvg:diff sum:sum];
            break;
        }
        obj = nil;
    }
    DebugLog(@"avg %f",avg);
    return avg;
}
-(double)calcAvg:(double)ldifference sum:(double)lsum
{
    if (lsum == 0)
    {
        return 0;
    }
    else
    {
        return (ldifference / lsum);
    }
}
-(void)addTotal
{
    double fuelSum = 0;
    double costSum = 0;
    if(sqlite3_open([[NEONAppDelegate databasePath] UTF8String], &expenseDB) == SQLITE_OK)
    {
        //NSString *findSQL = [NSString stringWithFormat:@"select ID,MILEAGEID,ODOMETER_READING,DATE,FUEL,ISPARTIAL,COST,FLAGTYPE from MILEAGE_BREAKS where MILEAGEID=\"%d\"", self.dbowID];
        NSString *findSQL = [NSString stringWithFormat:@"select SUM(FUEL),SUM(COST) from MILEAGE_BREAKS where MILEAGEID=\"%d\"", self.dbowID];
        const char *sqlStatement = [findSQL UTF8String];
		sqlite3_stmt *compiledStatement;
		if(sqlite3_prepare_v2(expenseDB, sqlStatement, -1, &compiledStatement, NULL) == SQLITE_OK) {
			
            while(sqlite3_step(compiledStatement) == SQLITE_ROW)
            {
				// Read the data from the result row
                fuelSum = sqlite3_column_double(compiledStatement, 0);
                costSum = sqlite3_column_double(compiledStatement, 1);
				
                DebugLog(@"fuelSum %f -costSum %f ",fuelSum,costSum);
			}
		}
		// Release the compiled statement from memory
		sqlite3_finalize(compiledStatement);
	}
	sqlite3_close(expenseDB);
    
    FuelDetail *fuelFirstObj = (FuelDetail *)[fuelArray objectAtIndex:0];
    FuelDetail *fuelLastObj = (FuelDetail *)[fuelArray objectAtIndex:fuelArray.count-1];

    NSDate *startDate = [NSDate dateWithTimeIntervalSinceReferenceDate:fuelFirstObj.date];
    NSDate *endDate = [NSDate dateWithTimeIntervalSinceReferenceDate:fuelLastObj.date];
    
    
    NSCalendar *gregorianCalendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
    NSDateComponents *components = [gregorianCalendar components:NSDayCalendarUnit
                                                        fromDate:startDate
                                                          toDate:endDate
                                                         options:0];
    [gregorianCalendar release];
    
    double odometerDiff = fuelLastObj.odometerReading - fuelFirstObj.odometerReading;
    double mileageAvg = [self calcAvg:odometerDiff sum:fuelSum];

    //    UILabel *label3 = [footerView.subviews objectAtIndex:1];
//    label3.text = [NSString stringWithFormat:@"%d",[components day]+1];
//    DebugLog(@"days = %d",[components day]);
//    
//    UILabel *label4 = [footerView.subviews objectAtIndex:2];
//    label4.text = [NSString stringWithFormat:@"%0.f", odometerDiff];
//    DebugLog(@"odometer diff = %f",odometerDiff);
    
//    UILabel *label1 = [footerView.subviews objectAtIndex:3];
//    label1.text = [NSString stringWithFormat:@"%0.f", fuelSum];
//    DebugLog(@"fuelSum = %f",fuelSum);
//
//    UILabel *label2 = [footerView.subviews objectAtIndex:4];
//    label2.text = [NSString stringWithFormat:@"%0.f", costSum];
//    DebugLog(@"costSum = %f",costSum);

    dayslbl.text = [NSString stringWithFormat:@"%d",[components day]+1];
    DebugLog(@"days = %d",[components day]);

    odometerSumlbl.text = [NSString stringWithFormat:@"%.1f", odometerDiff];
    DebugLog(@"odometer diff = %f",odometerDiff);
    
    fuelSumlbl.text = [NSString stringWithFormat:@"%.1f", fuelSum];
    DebugLog(@"fuelSum = %f",fuelSum);
    
    costSumlbl.text = [NSString stringWithFormat:@"%.1f", costSum];
    DebugLog(@"costSum = %f",costSum);
    
    [self.calcMileageBtn setHidden:TRUE];
    [self.mileageBoxView setHidden:FALSE];
    mileageBoxlbl.text = [NSString stringWithFormat:@"%.2f", mileageAvg];
    DebugLog(@"mileageAvg = %f",mileageAvg);
}

-(void)updateAvgMileageReading
{
    double fuelSum = 0;
    double costSum = 0;
    if(sqlite3_open([[NEONAppDelegate databasePath] UTF8String], &expenseDB) == SQLITE_OK)
    {
        //NSString *findSQL = [NSString stringWithFormat:@"select ID,MILEAGEID,ODOMETER_READING,DATE,FUEL,ISPARTIAL,COST,FLAGTYPE from MILEAGE_BREAKS where MILEAGEID=\"%d\"", self.dbowID];
        NSString *findSQL = [NSString stringWithFormat:@"select SUM(FUEL),SUM(COST) from MILEAGE_BREAKS where MILEAGEID=\"%d\"", self.dbowID];
        const char *sqlStatement = [findSQL UTF8String];
		sqlite3_stmt *compiledStatement;
		if(sqlite3_prepare_v2(expenseDB, sqlStatement, -1, &compiledStatement, NULL) == SQLITE_OK) {
			
            while(sqlite3_step(compiledStatement) == SQLITE_ROW)
            {
				// Read the data from the result row
                fuelSum = sqlite3_column_double(compiledStatement, 0);
                costSum = sqlite3_column_double(compiledStatement, 1);
				
                DebugLog(@"fuelSum %1.f -costSum %1.f ",fuelSum,costSum);
			}
		}
		// Release the compiled statement from memory
		sqlite3_finalize(compiledStatement);
    
    FuelDetail *fuelFirstObj = (FuelDetail *)[fuelArray objectAtIndex:0];
    FuelDetail *fuelLastObj = (FuelDetail *)[fuelArray objectAtIndex:fuelArray.count-1];
    
    double odometerDiff = fuelLastObj.odometerReading - fuelFirstObj.odometerReading;
    
    double mileageAvg = [self calcAvg:odometerDiff sum:fuelSum];
    
    sqlite3_stmt  *statement;
    
        NSString *updateSQL = [NSString stringWithFormat:@"update MILEAGE set AVGMILEAGE=\"%f\",COST=\"%f\" where id=\"%d\"", mileageAvg , costSum ,self.dbowID];
        const char *update_stmt = [updateSQL UTF8String];
        sqlite3_prepare_v2(expenseDB, update_stmt, -1, &statement, NULL);
        if (sqlite3_step(statement) == SQLITE_DONE)
        {
            DebugLog(@"Mileage Avg updated");
        } else {
            DebugLog(@"Mileage Avg update failed");
        }
        sqlite3_finalize(statement);
    }
    sqlite3_close(expenseDB);

}
@end
