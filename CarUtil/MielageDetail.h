//
//  MielageDetail.h
//  CarUtil
//
//  Created by Rishi Saxena on 18/05/12.
//  Copyright (c) 2012 Phonethics. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface MielageDetail : NSObject {
    NSInteger mileageid;
    double startdate;
    double enddate;
    double startreading;
    double endreading;
    double avgMileage;
    double cost;
    double average;
    NSString *selected;
    
}
@property (nonatomic, readwrite) NSInteger mileageid;
@property (nonatomic, readwrite) double startdate;
@property (nonatomic, readwrite) double enddate;
@property (nonatomic, readwrite) double startreading;
@property (nonatomic, readwrite) double endreading;
@property (nonatomic, readwrite) double avgMileage;
@property (nonatomic, readwrite) double average;
@property (nonatomic, readwrite) double cost;
@property (nonatomic, copy) NSString *selected;
@property (nonatomic, readwrite) NSInteger flagType;

-(id)initWithID:(NSInteger)lid  startdate:(double)sdate enddate:(double)edate startreading:(double)sreading endreading:(double)ereading avgMileage:(double)lfuel average:(double)lavg cost:(double)lcost selected:(NSString *)lselected flagType:(NSInteger)lflagType;

@end
