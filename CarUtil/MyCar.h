//
//  MyCar.h
//  CarUtil
//
//  Created by Sagar Mody on 10/06/12.
//  Copyright (c) 2012 Phonethics. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MyCar : NSObject {
    NSInteger myCarid;      //Car ID
    NSString *imgName;      //Image ID
    NSString *brand;        //Brand
    NSString *model;        //Model
    NSString *carnumber;    //License Plate No.
    double mfgdate;         //Manufacturing Date
	NSString *RCno;         //RC Number
    NSString *insurername;  //Insured Person
    NSString *policyno;     //Insurance Policy Number 
    double policydate;      //Policy Date
    NSString *chassisno;    //Chassis Number
    double purchasedate;    //Purchase Date
    NSString *selected;
}
@property (nonatomic, readwrite) NSInteger myCarid;
@property (nonatomic, copy) NSString *imgName;
@property (nonatomic, copy) NSString *brand;
@property (nonatomic, copy) NSString *model;
@property (nonatomic, copy) NSString *carnumber;
@property (nonatomic, readwrite) double mfgdate;
@property (nonatomic, copy) NSString *RCno;
@property (nonatomic, copy) NSString *insurername;
@property (nonatomic, copy) NSString *policyno;
@property (nonatomic, readwrite) double policydate;
@property (nonatomic, copy) NSString *chassisno;
@property (nonatomic, readwrite) double purchasedate;
@property (nonatomic, copy) NSString *selected;

-(id)initWithID:(NSInteger)lmyCarid  imgName:(NSString *)limgName brand:(NSString *)lcarbrand model:(NSString *)lcarmodel carnumber:(NSString *)lcarno mfgdate:(double)lmfgdate RCno:(NSString *)lRCno insurername:(NSString *)linsurername policyno:(NSString *)lpolicyno policydate:(double)ldate chassis:(NSString *)lchassisno   purchasedate:(double)lpurchasedate selected:(NSString *)lselectedcar;

@end
