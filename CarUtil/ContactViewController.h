//
//  ContactViewController.h
//  CarUtil
//
//  Created by Rishi Saxena on 09/04/12.
//  Copyright (c) 2012 Phonethics. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MessageUI/MFMailComposeViewController.h>

#define TESTDRIVE_TEL_TEXT @"Book a Test Drive"
#define CUSTSUPPORT_TEL_TEXT @"Call Nissan Customer Support"
#define CUSTSUPPORT_EMAIL_TEXT @"Email Customer Support"
#define BREAKDOWN_TEL_TEXT @"Call breakdown Helpline"
#define BREAKDOWN_EMAIL_TEXT @"Email breakdown location"
#define FACEBOOK_TEXT @"Facebook Connect"
#define DEALER_TEXT @"Dealer Locator"


#define TESTDRIVE_TEL @"tel://18002094080"
#define CUSTSUPPORT_TEL @"tel://18002094080"
#define CUSTSUPPORT_EMAILID @"customercare.nissan@hai.net.in"
#define BREAKDOWN_TEL @"tel://18002094080"

@interface ContactViewController : UIViewController <MFMailComposeViewControllerDelegate>{
}
@property (retain, nonatomic) IBOutlet UIImageView *bgImageView;
@property (strong, nonatomic) NSArray *contactList;
@property (retain, nonatomic) IBOutlet UITableView *contactTableView;
@end
