//
//  ExpenseType.m
//  CarUtil
//
//  Created by Rishi Saxena on 04/05/12.
//  Copyright (c) 2012 Phonethics. All rights reserved.
//

#import "ExpenseType.h"

@implementation ExpenseType
@synthesize  type,expenseTypeid;

-(id)initWithID:(NSInteger)typid  type:(NSString *)ltype 
{
    self = [super init];
    
    if(self) {
        self.expenseTypeid = typid;
        self.type = ltype;
    }
	return self;
}

-(void)dealloc
{
    //[self.type release];
    self.type = nil;
    [super dealloc];
}

@end
