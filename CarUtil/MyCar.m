//
//  MyCar.m
//  CarUtil
//
//  Created by Sagar Mody on 10/06/12.
//  Copyright (c) 2012 Phonethics. All rights reserved.
//

#import "MyCar.h"

@implementation MyCar
@synthesize myCarid ;
@synthesize imgName ;
@synthesize brand ;
@synthesize model ;
@synthesize carnumber ;
@synthesize mfgdate ;
@synthesize RCno ;
@synthesize insurername ;
@synthesize policyno ;
@synthesize policydate ;
@synthesize chassisno ;
@synthesize purchasedate ;
@synthesize selected ;

-(id)initWithID:(NSInteger)lmyCarid  imgName:(NSString *)limgName brand:(NSString *)lcarbrand model:(NSString *)lcarmodel carnumber:(NSString *)lcarno mfgdate:(double)lmfgdate RCno:(NSString *)lRCno insurername:(NSString *)linsurername policyno:(NSString *)lpolicyno policydate:(double)ldate chassis:(NSString *)lchassisno   purchasedate:(double)lpurchasedate selected:(NSString *)lselectedcar
{
    self = [super init];
    
    if(self) {
        self.myCarid = lmyCarid;
        self.imgName = limgName;
        self.brand = lcarbrand;
        self.model = lcarmodel;
        self.carnumber = lcarno;
        self.mfgdate = lmfgdate;
        self.RCno = lRCno;
        self.insurername = linsurername;
        self.policyno = lpolicyno;
        self.policydate = ldate;
        self.chassisno = lchassisno;
        self.purchasedate = lpurchasedate;
        self.selected = lselectedcar;
    }
	return self;
}

-(void)dealloc
{
    self.imgName = nil;
    self.brand = nil;
    self.model = nil;
    self.carnumber = nil;
    self.RCno = nil;
    self.insurername = nil;
    self.policyno = nil;
	self.chassisno = nil;
    self.selected = nil;
    [super dealloc];
}


@end
