//
//  MileageViewController.m
//  CarUtil
//
//  Created by Sagar Mody on 12/05/12.
//  Copyright (c) 2012 Phonethics. All rights reserved.
//

#import "MileageViewController.h"
//#import "AddMileageViewController.h"
#import "MielageDetail.h"
#import "CarUtilAppDelegate.h"
#import "FuelDetailViewController.h"
#import "AddEcditFuelViewController.h"
#import "MyCarViewController.h"

@interface MileageViewController ()

@end

@implementation MileageViewController
@synthesize mileageTable;
@synthesize mileageArray;
@synthesize footerView;
@synthesize headerView;
@synthesize cardId;
@synthesize infoBtn;
@synthesize activeSessionRowId;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}


#pragma mark Table view methods
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 49;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section   // custom view for header. will be adjusted to default or specified header height
{
    
    if(headerView == nil) {
        //allocate the view if it doesn't exist yet
        headerView  = [[UIView alloc] initWithFrame:CGRectMake(0.0f,0.0f,300.0f,49)];
        //headerView.backgroundColor = [UIColor lightGrayColor];
        
        UIImage *image = [UIImage imageNamed:@"add.png"];
        
        UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
        CGRect frame = CGRectMake(0.0, 0.0, 32, 49);
        button.frame = frame;	// match the button's size with the image size
		button.tag = 4;
        [button setBackgroundImage:image forState:UIControlStateNormal];
        // set the button's target to this table view controller so we can interpret touch events and map that to a NSIndexSet
        [button addTarget:self action:@selector(checkAllMileageButtonTapped:event:) forControlEvents:UIControlEventTouchUpInside];
        //------------------------------------------------------------------------------------------------------------------------//       
        
        UIImage *image1 = [UIImage imageNamed:@"headings.png"];
        UIImageView *tempImg = [[[UIImageView alloc] initWithFrame:CGRectMake(32, 0, 270, 49)] autorelease];
        [tempImg setImage:image1];
        
        //add the button to the view
        [headerView insertSubview:button atIndex:0];
        [headerView insertSubview:tempImg atIndex:1];

    }
    headerView.opaque = TRUE;
    //return the view for the footer
    return headerView;
}

// specify the height of your footer section
- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    //differ between your sections or if you
    //have only on section return a static value
    return 49;
}

// custom view for footer. will be adjusted to default or specified footer height
// Notice: this will work only for one section within the table view
- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section {
    
    if(footerView == nil) {
        //allocate the view if it doesn't exist yet
        footerView  = [[UIView alloc] init];
        footerView.backgroundColor = [UIColor clearColor];
        CGRect rect = CGRectMake(170.0f,0.0f,80.0f,49.0f);
        
        UIImage *image = [UIImage imageNamed:@"delete.png"];
        
        UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
        CGRect frame = CGRectMake(0.0, 0.0, 32, 49);
        button.frame = frame;	// match the button's size with the image size
		button.tag = 4;
        [button setBackgroundImage:image forState:UIControlStateNormal];
        //button.imageView.tag = 11;
        // set the button's target to this table view controller so we can interpret touch events and map that to a NSIndexSet
        [button addTarget:self action:@selector(cancelAllMileageButtonTapped:event:) forControlEvents:UIControlEventTouchUpInside];
        
        UILabel* label  = [[[UILabel alloc] initWithFrame:rect] autorelease];
        label.backgroundColor   = [UIColor clearColor];
        label.text      =  @"0";
        label.baselineAdjustment= UIBaselineAdjustmentAlignCenters;
        label.lineBreakMode =  UILineBreakModeTailTruncation;
        label.textAlignment = UITextAlignmentCenter;
        //label.shadowColor   = [UIColor whiteColor];
        //label.shadowOffset  = CGSizeMake(0.0, 1.0);
        label.font      = [UIFont systemFontOfSize:20];
        label.textColor     = [UIColor whiteColor];
        label.numberOfLines = 0;
        
       
        UIImageView *tempImg = [[[UIImageView alloc] initWithFrame:CGRectMake(32, 0, 268, 49)] autorelease];
        [tempImg setImage:[UIImage imageNamed:@"totalavg.png"]];
        
        //add the button to the view
        [footerView insertSubview:tempImg atIndex:0];
        [footerView insertSubview:button atIndex:2];
        [footerView insertSubview:label atIndex:1];
    }
    
    //return the view for the footer
    return footerView;
}


- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 60;
}

// Customize the number of rows in the table view.
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [mileageArray count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *CellIdentifier = @"Cell";
    UILabel *lblstartDateset;
    UILabel *lblstartkmset;
    UILabel *lblendDateset;
    UILabel *lblendkmset;
    UILabel *lblavgset;
    UILabel *lblcostset;

    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier] autorelease];

        UIImage *image = [UIImage imageNamed:@"un-check.png"];
        
        UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
        CGRect frame = CGRectMake(cell.frame.origin.x , cell.frame.origin.y + 15 , image.size.width, image.size.height);
        button.frame = frame;	// match the button's size with the image size
		button.tag = 1;
        [button setBackgroundImage:image forState:UIControlStateNormal];
        button.imageView.tag = 10;
        // set the button's target to this table view controller so we can interpret touch events and map that to a NSIndexSet
        [button addTarget:self action:@selector(getMileageButtonTapped:event:) forControlEvents:UIControlEventTouchUpInside];
        cell.backgroundColor = [UIColor clearColor];
        [cell.contentView addSubview:button];
        
        //Initialize Label with tag 2.(start date Label)
        lblstartDateset = [[UILabel alloc] initWithFrame:CGRectMake(cell.frame.origin.x + 35, cell.frame.origin.y+10, 70, 15)];
        lblstartDateset.tag = 2;
        //lblstartDateset.text = @"12/04/12";
        lblstartDateset.font = [UIFont boldSystemFontOfSize:12];
        lblstartDateset.textColor = [UIColor whiteColor];
        lblstartDateset.textAlignment = UITextAlignmentCenter;
        lblstartDateset .lineBreakMode =  UILineBreakModeTailTruncation;
        lblstartDateset.backgroundColor =  [UIColor clearColor];
        [cell.contentView addSubview:lblstartDateset];
        [lblstartDateset release];
        
        //Initialize Label with tag 3.(start km Label)
        lblstartkmset = [[UILabel alloc] initWithFrame:CGRectMake(cell.frame.origin.x + 35, cell.frame.origin.y+32, 65, 15)];
        lblstartkmset.tag = 3;
        //lblstartkmset.text = @"468423";
        lblstartkmset.font = [UIFont boldSystemFontOfSize:16];
        lblstartkmset.textColor = [UIColor whiteColor];
        lblstartkmset.textAlignment = UITextAlignmentCenter;
        lblstartkmset .lineBreakMode =  UILineBreakModeTailTruncation;
        lblstartkmset.backgroundColor =  [UIColor clearColor];
        [cell.contentView addSubview:lblstartkmset];
        [lblstartkmset release];
        
        
        //Initialize Label with tag 4.(end date Label)
        lblendDateset = [[UILabel alloc] initWithFrame:CGRectMake(cell.frame.origin.x + 110, cell.frame.origin.y+10, 70, 15)];
        lblendDateset.tag = 4;
        //lblendDateset.text = @"12/05/12";
        lblendDateset.font = [UIFont boldSystemFontOfSize:12];
        lblendDateset.textColor = [UIColor whiteColor];
        lblendDateset.textAlignment = UITextAlignmentCenter;
        lblendDateset .lineBreakMode =  UILineBreakModeTailTruncation;
        lblendDateset.backgroundColor =  [UIColor clearColor];
        [cell.contentView addSubview:lblendDateset];
        [lblendDateset release];
        
        //Initialize Label with tag 5.(end km Label)
        lblendkmset = [[UILabel alloc] initWithFrame:CGRectMake(cell.frame.origin.x + 113, cell.frame.origin.y+32, 65, 15)];
        lblendkmset.tag = 5;
        //lblendkmset.text = @"468433";
        lblendkmset.font = [UIFont boldSystemFontOfSize:16];
        lblendkmset.textColor = [UIColor whiteColor];
        lblendkmset.textAlignment = UITextAlignmentCenter;
        lblendkmset .lineBreakMode =  UILineBreakModeTailTruncation;
        lblendkmset.backgroundColor =  [UIColor clearColor];
        [cell.contentView addSubview:lblendkmset];
        [lblendkmset release];
        
       
        //Initialize Label with tag 6.(Avg Label)
        lblavgset = [[UILabel alloc] initWithFrame:CGRectMake(cell.frame.origin.x + 188 , cell.frame.origin.y + 24, 50, 15)];
        lblavgset.tag = 6;
        //lblavgset.text = @"20.05";
        lblavgset.font = [UIFont boldSystemFontOfSize:15];
        lblavgset.textColor = [UIColor whiteColor];
        lblavgset.textAlignment = UITextAlignmentCenter;
        lblavgset .lineBreakMode =  UILineBreakModeTailTruncation;
        lblavgset.backgroundColor =  [UIColor clearColor];
        [cell.contentView addSubview:lblavgset];
        [lblavgset release];
        
        
        //Initialize Label with tag 7.(Cost Label)
        lblcostset = [[UILabel alloc] initWithFrame:CGRectMake(lblavgset.frame.origin.x + lblavgset.frame.size.width + 8, cell.frame.origin.y+24, 52, 15)];
        lblcostset.tag = 7;
        //lblcostset.text = @"200";
        lblcostset.font = [UIFont boldSystemFontOfSize:15];
        lblcostset.textColor = [UIColor whiteColor];
        lblcostset.textAlignment = UITextAlignmentCenter;
        lblcostset .lineBreakMode =  UILineBreakModeTailTruncation;
        lblcostset.backgroundColor =  [UIColor clearColor];
        [cell.contentView addSubview:lblcostset];
        [lblcostset release];
        
    }
    
	MielageDetail *mileageObj = (MielageDetail *)[mileageArray objectAtIndex:indexPath.row];
    DebugLog(@" %d - %f - %f - %f - %f - %f - %f - %f - %@",mileageObj.mileageid, mileageObj.startdate, mileageObj.enddate, mileageObj.startreading,mileageObj.endreading, mileageObj.avgMileage, mileageObj.cost, mileageObj.average,mileageObj.selected);

    NSDate *startpickerDate = [NSDate dateWithTimeIntervalSinceReferenceDate:mileageObj.startdate ]; 
    NSDateFormatter *sdateFormat = [[NSDateFormatter alloc] init];
    [sdateFormat setDateFormat:@"dd MMM yyyy"];
    NSString *sdateString = [sdateFormat stringFromDate:startpickerDate];  
    lblstartDateset = (UILabel *)[cell viewWithTag:2];
    lblstartDateset.text = sdateString;
    [sdateFormat release];
    
    lblstartkmset = (UILabel *)[cell viewWithTag:3];
    lblstartkmset.text = [[NSNumber numberWithDouble:mileageObj.startreading] stringValue];
    //  cell.accessoryType = UITableViewCellAccessoryNone;
    cell.backgroundColor = [UIColor clearColor];
    UIImageView *tempImg = [[UIImageView alloc] initWithFrame:CGRectMake(0, 5, 320, 50)];
    
    if (mileageObj.mileageid == activeSessionRowId) 
    {
        [tempImg setImage:[UIImage imageNamed:@"active_table.png"]];
        
        lblendDateset       =   (UILabel *)[cell viewWithTag:4];
        lblendDateset.text  =   @"";
        
        lblendkmset         =   (UILabel *)[cell viewWithTag:5];
        lblendkmset.text    =   @"";
        
        lblavgset           =   (UILabel *)[cell viewWithTag:6];
        lblavgset.text      =   @"";
        
        lblcostset          =   (UILabel *)[cell viewWithTag:7];
        lblcostset.text     =   @"";
    }
    else
    {
        [tempImg setImage:[UIImage imageNamed:@"deactive_table.png"]];
        DebugLog(@"%d",(int)mileageObj.enddate);
        if(0 == (int)mileageObj.enddate) {
            lblendDateset = (UILabel *)[cell viewWithTag:4];
            lblendDateset.text = @"";
        } else {
            NSDate *endpickerDate = [NSDate dateWithTimeIntervalSinceReferenceDate:mileageObj.enddate ];
            NSDateFormatter *edateFormat = [[NSDateFormatter alloc] init];
            [edateFormat setDateFormat:@"dd MMM yyyy"];
            NSString *edateString = [edateFormat stringFromDate:endpickerDate];
            lblendDateset = (UILabel *)[cell viewWithTag:4];
            lblendDateset.text = edateString;
            [edateFormat release];
        }
        
        if(0 == (int)mileageObj.endreading) {
            lblendkmset = (UILabel *)[cell viewWithTag:5];
            lblendkmset.text = @"";
        } else {
            lblendkmset = (UILabel *)[cell viewWithTag:5];
            lblendkmset.text = [NSString stringWithFormat:@"%.1f",mileageObj.endreading];
        }
        
        lblavgset = (UILabel *)[cell viewWithTag:6];
        lblavgset.text = [NSString stringWithFormat:@"%.2f",mileageObj.avgMileage];
        
        lblcostset = (UILabel *)[cell viewWithTag:7];
        lblcostset.text = [[NSNumber numberWithDouble:mileageObj.cost] stringValue];
    }
        
       //cell.detailTextLabel.text = dateString;
        
        UIButton *button = (UIButton *)[cell viewWithTag:1];
        if([mileageObj.selected isEqualToString:@"1"] && mileageObj.flagType == END)
        {
            [button setBackgroundImage:[UIImage imageNamed:@"check.png"] forState:UIControlStateNormal];
        }
        else
        {
            [button setBackgroundImage:[UIImage imageNamed:@"un-check.png"] forState:UIControlStateNormal];
        }

    cell.backgroundView = tempImg;
    [tempImg release];
    return cell;
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    MielageDetail *mileageObj = (MielageDetail *)[mileageArray objectAtIndex:indexPath.row];
   // [self editMileageData:mileageObj.mileageid rowID:indexPath.row];
    [self editMileageBreaksData:mileageObj.mileageid rowID:indexPath.row];
    [mileageTable deselectRowAtIndexPath:[mileageTable indexPathForSelectedRow] animated:NO];
}

- (void)tableView:(UITableView *)tv commitEditingStyle:(UITableViewCellEditingStyle)editingStyle 
forRowAtIndexPath:(NSIndexPath *)indexPath {
	
	if(editingStyle == UITableViewCellEditingStyleDelete) {
		
		//Get the object to delete from the array.
        MielageDetail *mileageObj = (MielageDetail *)[mileageArray objectAtIndex:indexPath.row];
        //
        [self deleteMileagedata:mileageObj.mileageid];
        [self DeleteMilegaeBreakReading:mileageObj.mileageid];
		[mileageArray removeObject:mileageObj];
        if (activeSessionRowId == mileageObj.mileageid) {
            activeSessionRowId = -1;
        }
		
		//Delete the object from the table.
		[self.mileageTable deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationMiddle];
	}
}

- (void)getMileageButtonTapped:(id)sender event:(id)event
{
	NSSet *touches = [event allTouches];
	UITouch *touch = [touches anyObject];
	CGPoint currentTouchPosition = [touch locationInView:self.mileageTable];
	NSIndexPath *indexPath = [self.mileageTable indexPathForRowAtPoint: currentTouchPosition];
	if (indexPath != nil)
	{
        UITableViewCell *cell  = (UITableViewCell *)[self.mileageTable cellForRowAtIndexPath:indexPath];
        UIButton *button = (UIButton *)[cell viewWithTag:1];
        MielageDetail *mileageObj = (MielageDetail *)[mileageArray objectAtIndex:indexPath.row];
        if(mileageObj.flagType != END)
            return;
        if([mileageObj.selected isEqualToString:@"0"]){
            
            // Create a new expense object with the data from the database
            MielageDetail *tempObj = [[MielageDetail alloc] initWithID:mileageObj.mileageid startdate:mileageObj.startdate enddate:mileageObj.enddate startreading:mileageObj.startreading endreading:mileageObj.endreading avgMileage:mileageObj.avgMileage average:mileageObj.average cost:mileageObj.cost selected:@"1" flagType:mileageObj.flagType];
            [mileageArray replaceObjectAtIndex:indexPath.row withObject:tempObj];
            [button setBackgroundImage:[UIImage imageNamed:@"check.png"] forState:UIControlStateNormal];
            [self updateRowButtonSelection:tempObj.mileageid selected:@"1"];
            [tempObj release];
        } else {
            // Create a new expense object with the data from the database
            MielageDetail *tempObj = [[MielageDetail alloc] initWithID:mileageObj.mileageid startdate:mileageObj.startdate enddate:mileageObj.enddate startreading:mileageObj.startreading endreading:mileageObj.endreading avgMileage:mileageObj.avgMileage average:mileageObj.average cost:mileageObj.cost selected:@"0" flagType:mileageObj.flagType];
            [mileageArray replaceObjectAtIndex:indexPath.row withObject:tempObj];
            [button setBackgroundImage:[UIImage imageNamed:@"un-check.png"] forState:UIControlStateNormal];
            [self updateRowButtonSelection:tempObj.mileageid selected:@"0"];
            [tempObj release];
            
        }
        
	}
    
}

-(void)updateRowButtonSelection:(NSInteger)mileageId selected:(NSString *)value
{
    sqlite3_stmt    *statement;
    
    const char *dbpath = [[(CarUtilAppDelegate *)[[UIApplication sharedApplication] delegate] databasePath] UTF8String];
    
    if (sqlite3_open(dbpath, &expenseDB) == SQLITE_OK)
    {
        NSString *updateSQL = [NSString stringWithFormat:@"update MILEAGE set selected=\"%@\"  where id=\"%d\"",  value , mileageId];
        const char *update_stmt = [updateSQL UTF8String];
        sqlite3_prepare_v2(expenseDB, update_stmt, -1, &statement, NULL);
        if (sqlite3_step(statement) == SQLITE_DONE)
        {
            //[self showMessage:@"Contact updated"];
            //[self clearData:nil];
        } else {
            //[self showMessage:@"Contact updatation failed"];
        }
        sqlite3_finalize(statement);
    }         
    sqlite3_close(expenseDB);
    [self addTotalMileageAvg];
    
}
-(void)addTotalMileageAvg
{
    sqlite3_stmt    *statement;
    double totalMileage = 0;
    int rowcount = 0;
    const char *dbpath = [[(CarUtilAppDelegate *)[[UIApplication sharedApplication] delegate] databasePath] UTF8String];
    
    if (sqlite3_open(dbpath, &expenseDB) == SQLITE_OK)
    {
        NSString *querySQLtotalkm = [NSString stringWithFormat:@"SELECT count(*),sum(AVGMILEAGE) FROM MILEAGE WHERE selected=\"%@\" AND FLAGTYPE=\"%d\" AND CARID=\"%d\"", @"1" , END ,self.cardId];
        
        const char *query_stmt_km = [querySQLtotalkm UTF8String];
        
        if (sqlite3_prepare_v2(expenseDB, query_stmt_km, -1, &statement, NULL) == SQLITE_OK)
        {
            if (sqlite3_step(statement) == SQLITE_ROW)
            {
                rowcount =  sqlite3_column_int(statement, 0);
                totalMileage =  sqlite3_column_double(statement, 1);
                //UILabel *label = [footerView.subviews objectAtIndex:1];
                //label.text = [NSString stringWithFormat:@"%d", field1];
                
            } else
            {
                //Failed
            }
            sqlite3_finalize(statement);
        }
        DebugLog(@"total KM = %f row = %d result = %.2f",totalMileage,rowcount,totalMileage/rowcount);

        UILabel *label = [footerView.subviews objectAtIndex:1];
        if(rowcount!= 0)
            label.text = [NSString stringWithFormat:@"%.2f", totalMileage/rowcount];
        else
            label.text = @"0.0";
        DebugLog(@"AVERAGE = %@",label.text);
        
        sqlite3_close(expenseDB);
    }
}

-(void)addTotalAvgCost
{
    sqlite3_stmt    *statementkm;
    sqlite3_stmt    *statementfuel;
    int totalKM = 0;
    int totalfuel = 0;
    int totalAVG = 0;
    
    const char *dbpath = [[(CarUtilAppDelegate *)[[UIApplication sharedApplication] delegate] databasePath] UTF8String];
    
    if (sqlite3_open(dbpath, &expenseDB) == SQLITE_OK)
    {
        NSString *querySQLtotalkm = [NSString stringWithFormat:@"SELECT sum(TOTALKM) as TOTAL FROM MILEAGE WHERE selected=\"%@\" AND TOTALKM > \"%d\" AND AVERAGE > \"%d\" AND CARID=\"%d\"", @"1" , 0 , 0 , self.cardId];
        
        const char *query_stmt_km = [querySQLtotalkm UTF8String];
        
        if (sqlite3_prepare_v2(expenseDB, query_stmt_km, -1, &statementkm, NULL) == SQLITE_OK)
        {
            if (sqlite3_step(statementkm) == SQLITE_ROW)
            {
                totalKM =  sqlite3_column_int(statementkm, 0);
                //UILabel *label = [footerView.subviews objectAtIndex:1];
                //label.text = [NSString stringWithFormat:@"%d", field1];
                //DebugLog(@"total KM = %d",field1);
                
            } else {
                //Failed
            }
            sqlite3_finalize(statementkm);
        }
        
        NSString *querySQLtotalfuel = [NSString stringWithFormat:@"SELECT sum(FUEL) as TOTAL FROM MILEAGE WHERE selected=\"%@\" AND TOTALKM > \"%d\" AND AVERAGE > \"%d\" AND CARID=\"%d\"", @"1" , 0 , 0 , self.cardId];
        
        const char *query_stmt_fuel = [querySQLtotalfuel UTF8String];
        
        if (sqlite3_prepare_v2(expenseDB, query_stmt_fuel, -1, &statementfuel, NULL) == SQLITE_OK)
        {
            if (sqlite3_step(statementfuel) == SQLITE_ROW)
            {
                totalfuel =  sqlite3_column_int(statementfuel, 0);
                //UILabel *label = [footerView.subviews objectAtIndex:1];
                //label.text = [NSString stringWithFormat:@"%d", field1];
                //DebugLog(@"fuel = %d",field2);
                
            } else {
                //Failed
            }
            sqlite3_finalize(statementfuel);
        }
        if(totalKM > 0 && totalfuel > 0)
            totalAVG = totalKM / totalfuel;
        UILabel *label = [footerView.subviews objectAtIndex:1];
        label.text = [NSString stringWithFormat:@"%d", totalAVG];
        //DebugLog(@"AVERAGE = %d",totalAVG);

        
        sqlite3_close(expenseDB);
    }
}

-(void)updateMileageAllRowButtonSelection:(NSString *)selectedvalue
{
    sqlite3_stmt    *statement;
    
    const char *dbpath = [[(CarUtilAppDelegate *)[[UIApplication sharedApplication] delegate] databasePath] UTF8String];
    
    if (sqlite3_open(dbpath, &expenseDB) == SQLITE_OK)
    {
        NSString *updateSQL = [NSString stringWithFormat:@"update MILEAGE set selected=\"%@\" where CARID=\"%d\"",  selectedvalue, self.cardId ];
        const char *update_stmt = [updateSQL UTF8String];
        sqlite3_prepare_v2(expenseDB, update_stmt, -1, &statement, NULL);
        if (sqlite3_step(statement) == SQLITE_DONE)
        {
            //[self showMessage:@"Contact updated"];
            //[self clearData:nil];
        } else {
            //[self showMessage:@"Contact updatation failed"];
        }
        sqlite3_finalize(statement);
    }         
    sqlite3_close(expenseDB);
}

-(void)deleteMileagedata:(NSInteger)expenseid
{
    sqlite3_stmt    *statement;
    
    const char *dbpath = [[(CarUtilAppDelegate *)[[UIApplication sharedApplication] delegate] databasePath] UTF8String];
    
    if (sqlite3_open(dbpath, &expenseDB) == SQLITE_OK)
    {
        NSString *deleteSQL = [NSString stringWithFormat:@"DELETE FROM MILEAGE WHERE id=\"%d\"", expenseid];
        const char *delete_stmt = [deleteSQL UTF8String];
        sqlite3_prepare_v2(expenseDB, delete_stmt, -1, &statement, NULL);
        if (sqlite3_step(statement) == SQLITE_DONE)
        {
            //[self showMessage:@"Contact deleted"];
            //[self clearData:nil];
        } else {
            //[self showMessage:@"Failed to delete contact"];
        }
        sqlite3_finalize(statement);
    }         
    sqlite3_close(expenseDB);
    [self addTotalMileageAvg];
}

//- (void) editMileageData:(NSInteger)ldbrowID rowID:(NSInteger)lrowID
//{   
//  
//    AddMileageViewController *addmileageviewController = [[AddMileageViewController alloc] initWithNibName:@"AddMileageViewController" bundle:nil] ;
//    addmileageviewController.title = @"Mileage Meter";
//    //to push the UIView.
//    addmileageviewController.dbowID = ldbrowID;
//    addmileageviewController.rowID = lrowID;
//    addmileageviewController.hidesBottomBarWhenPushed = YES;
//    [self.navigationController pushViewController:addmileageviewController animated:YES];
//    [addmileageviewController release];
//}
- (void) editMileageBreaksData:(NSInteger)ldbrowID rowID:(NSInteger)lrowID
{
    
    FuelDetailViewController *fueldetailController = [[FuelDetailViewController alloc] initWithNibName:@"FuelDetailViewController" bundle:nil] ;
    fueldetailController.title = @"Fuel Details";
    fueldetailController.dbowID = ldbrowID;
    fueldetailController.cardId = self.cardId;
    fueldetailController.hidesBottomBarWhenPushed = NO;
    [self.navigationController pushViewController:fueldetailController animated:YES];
    [fueldetailController release];
}

- (void)checkAllMileageButtonTapped:(id)sender event:(id)event
{ 
    [mileageArray removeAllObjects];
    [self updateMileageAllRowButtonSelection:@"1"];
    [self readMileageFromDatabase];
    [self sortMileageArraybyStartDate];
    [mileageTable reloadData];
    [self addTotalMileageAvg];
}
- (void)cancelAllMileageButtonTapped:(id)sender event:(id)event 
{
    [mileageArray removeAllObjects];
    [self updateMileageAllRowButtonSelection:@"0"];
    [self readMileageFromDatabase];
    [self sortMileageArraybyStartDate];
    [mileageTable reloadData];
    [self addTotalMileageAvg];
    
}

-(void)sortMileageArraybyStartDate
{
    NSSortDescriptor *alphaDesc = [[NSSortDescriptor alloc] initWithKey:@"startdate" ascending:NO selector:@selector(compare:)];
    [mileageArray sortUsingDescriptors:[NSMutableArray arrayWithObjects:alphaDesc, nil]];
    [alphaDesc release], alphaDesc = nil;
}


-(void)sortMileageArray 
{
    UIButton *avgbutton = [headerView.subviews objectAtIndex:0]; //avg button
    UIButton *dtendbutton = [headerView.subviews objectAtIndex:1]; //date end button
    UIButton *dtstartbutton = [headerView.subviews objectAtIndex:2]; //date start button
    UIButton *costbutton = [headerView.subviews objectAtIndex:4]; //cost button

    if(avgbutton.isSelected == YES) {
        DebugLog(@"avgbutton is selected");
        if(avgbutton.tag == 7 || avgbutton.tag == 7002)
        {
            NSSortDescriptor *alphaDesc = [[NSSortDescriptor alloc] initWithKey:@"average" ascending:YES selector:@selector(compare:)];
            [mileageArray sortUsingDescriptors:[NSMutableArray arrayWithObjects:alphaDesc, nil]];	
            [alphaDesc release], alphaDesc = nil;
        } else if(avgbutton.tag == 7001) {
            NSSortDescriptor *alphaDesc = [[NSSortDescriptor alloc] initWithKey:@"average" ascending:NO selector:@selector(compare:)];
            [mileageArray sortUsingDescriptors:[NSMutableArray arrayWithObjects:alphaDesc, nil]];	
            [alphaDesc release], alphaDesc = nil;
        } 
    } else if(dtendbutton.isSelected == YES) {
        DebugLog(@"dtendbutton is selected");
        if(dtendbutton.tag == 6 || dtendbutton.tag == 6002)
        {
            NSSortDescriptor *alphaDesc = [[NSSortDescriptor alloc] initWithKey:@"enddate" ascending:YES selector:@selector(compare:)];
            [mileageArray sortUsingDescriptors:[NSMutableArray arrayWithObjects:alphaDesc, nil]];	
            [alphaDesc release], alphaDesc = nil;
        } else if(dtendbutton.tag == 6001) {
            NSSortDescriptor *alphaDesc = [[NSSortDescriptor alloc] initWithKey:@"enddate" ascending:NO selector:@selector(compare:)];
            [mileageArray sortUsingDescriptors:[NSMutableArray arrayWithObjects:alphaDesc, nil]];	
            [alphaDesc release], alphaDesc = nil;
        } 
    } else if(costbutton.isSelected == YES) {
        DebugLog(@"costbutton is selected");
        if(costbutton.tag == 8 || costbutton.tag == 8002)
        {
            NSSortDescriptor *alphaDesc = [[NSSortDescriptor alloc] initWithKey:@"cost" ascending:YES selector:@selector(compare:)];
            [mileageArray sortUsingDescriptors:[NSMutableArray arrayWithObjects:alphaDesc, nil]];	
            [alphaDesc release], alphaDesc = nil;
        } else if(costbutton.tag == 8001) {
            NSSortDescriptor *alphaDesc = [[NSSortDescriptor alloc] initWithKey:@"cost" ascending:NO selector:@selector(compare:)];
            [mileageArray sortUsingDescriptors:[NSMutableArray arrayWithObjects:alphaDesc, nil]];	
            [alphaDesc release], alphaDesc = nil;
        } 
    } else {
        DebugLog(@"dtstartbutton is selected");
        if(dtstartbutton.tag == 5 || dtstartbutton.tag == 5002)
        {
            NSSortDescriptor *alphaDesc = [[NSSortDescriptor alloc] initWithKey:@"startdate" ascending:YES selector:@selector(compare:)];
            [mileageArray sortUsingDescriptors:[NSMutableArray arrayWithObjects:alphaDesc, nil]];	
            [alphaDesc release], alphaDesc = nil;
        } else if(dtstartbutton.tag == 5001) {
            NSSortDescriptor *alphaDesc = [[NSSortDescriptor alloc] initWithKey:@"startdate" ascending:NO selector:@selector(compare:)];
            [mileageArray sortUsingDescriptors:[NSMutableArray arrayWithObjects:alphaDesc, nil]];	
            [alphaDesc release], alphaDesc = nil;
        } 
    }
}

- (void)startdateButtonTapped:(id)sender event:(id)event
{
    UIButton *avgbutton = [headerView.subviews objectAtIndex:0]; //avg button
    UIButton *dtendbutton = [headerView.subviews objectAtIndex:1]; //date end button
    UIButton *dtstartbutton = [headerView.subviews objectAtIndex:2]; //date start button
    UIButton *costbutton = [headerView.subviews objectAtIndex:4]; //cost button
    if(dtstartbutton.tag == 5 || dtstartbutton.tag == 5002)
    {
        dtstartbutton.tag = 5001;
        [dtstartbutton setBackgroundImage:[UIImage imageNamed:@"date_start_act_down.png"] forState:UIControlStateNormal];
        //[self sortColumnButtonSelection:@"date" type:@"asc"];
        NSSortDescriptor *alphaDesc = [[NSSortDescriptor alloc] initWithKey:@"startdate" ascending:NO selector:@selector(compare:)];
        [mileageArray sortUsingDescriptors:[NSMutableArray arrayWithObjects:alphaDesc, nil]];	
        [alphaDesc release], alphaDesc = nil;
    } else if(dtstartbutton.tag == 5001) {
        dtstartbutton.tag = 5002;
        [dtstartbutton setBackgroundImage:[UIImage imageNamed:@"date_start_act_up.png"] forState:UIControlStateNormal];
        //[self sortColumnButtonSelection:@"date" type:@"desc"];
        NSSortDescriptor *alphaDesc = [[NSSortDescriptor alloc] initWithKey:@"startdate" ascending:YES selector:@selector(compare:)];
        [mileageArray sortUsingDescriptors:[NSMutableArray arrayWithObjects:alphaDesc, nil]];	
        [alphaDesc release], alphaDesc = nil;
    } 
    [mileageTable reloadData];
    [avgbutton setBackgroundImage:[UIImage imageNamed:@"avg_deact.png"] forState:UIControlStateNormal];
    [dtendbutton setBackgroundImage:[UIImage imageNamed:@"date_end_deact.png"] forState:UIControlStateNormal];
    [costbutton setBackgroundImage:[UIImage imageNamed:@"cost_deact.png"] forState:UIControlStateNormal];
    dtstartbutton.selected = YES;
    avgbutton.selected = NO;
    dtendbutton.selected = NO;
    costbutton.selected = NO;
}

- (void)enddateButtonTapped:(id)sender event:(id)event
{
    UIButton *avgbutton = [headerView.subviews objectAtIndex:0]; //avg button
    UIButton *dtendbutton = [headerView.subviews objectAtIndex:1]; //date end button
    UIButton *dtstartbutton = [headerView.subviews objectAtIndex:2]; //date start button
    UIButton *costbutton = [headerView.subviews objectAtIndex:4]; //cost button
    if(dtendbutton.tag == 6 || dtendbutton.tag == 6002)
    {
        dtendbutton.tag = 6001;
        [dtendbutton setBackgroundImage:[UIImage imageNamed:@"date_end_act_down.png"] forState:UIControlStateNormal];
        //[self sortColumnButtonSelection:@"date" type:@"asc"];
        NSSortDescriptor *alphaDesc = [[NSSortDescriptor alloc] initWithKey:@"enddate" ascending:NO selector:@selector(compare:)];
        [mileageArray sortUsingDescriptors:[NSMutableArray arrayWithObjects:alphaDesc, nil]];	
        [alphaDesc release], alphaDesc = nil;
    } else if(dtendbutton.tag == 6001) {
        dtendbutton.tag = 6002;
        [dtendbutton setBackgroundImage:[UIImage imageNamed:@"date_end_act_up.png"] forState:UIControlStateNormal];
        //[self sortColumnButtonSelection:@"date" type:@"desc"];
        NSSortDescriptor *alphaDesc = [[NSSortDescriptor alloc] initWithKey:@"enddate" ascending:YES selector:@selector(compare:)];
        [mileageArray sortUsingDescriptors:[NSMutableArray arrayWithObjects:alphaDesc, nil]];	
        [alphaDesc release], alphaDesc = nil;
    } 
    [avgbutton setBackgroundImage:[UIImage imageNamed:@"avg_deact.png"] forState:UIControlStateNormal];
    [dtstartbutton setBackgroundImage:[UIImage imageNamed:@"date_start_deact.png"] forState:UIControlStateNormal];
    [costbutton setBackgroundImage:[UIImage imageNamed:@"cost_deact.png"] forState:UIControlStateNormal];
    [mileageTable reloadData];
    dtstartbutton.selected = NO;
    avgbutton.selected = NO;
    dtendbutton.selected = YES;
    costbutton.selected = NO;
}

- (void)avgButtonTapped:(id)sender event:(id)event
{
    UIButton *avgbutton = [headerView.subviews objectAtIndex:0]; //avg button
    UIButton *dtendbutton = [headerView.subviews objectAtIndex:1]; //date end button
    UIButton *dtstartbutton = [headerView.subviews objectAtIndex:2]; //date start button
    UIButton *costbutton = [headerView.subviews objectAtIndex:4]; //cost button
    if(avgbutton.tag == 7 || avgbutton.tag == 7002)
    {
        avgbutton.tag = 7001;
        [avgbutton setBackgroundImage:[UIImage imageNamed:@"avg_act_down.png"] forState:UIControlStateNormal];
        //[self sortColumnButtonSelection:@"date" type:@"asc"];
        NSSortDescriptor *alphaDesc = [[NSSortDescriptor alloc] initWithKey:@"average" ascending:NO selector:@selector(compare:)];
        [mileageArray sortUsingDescriptors:[NSMutableArray arrayWithObjects:alphaDesc, nil]];	
        [alphaDesc release], alphaDesc = nil;
    } else if(avgbutton.tag == 7001) {
        avgbutton.tag = 7002;
        [avgbutton setBackgroundImage:[UIImage imageNamed:@"avg_act_up.png"] forState:UIControlStateNormal];
        //[self sortColumnButtonSelection:@"date" type:@"desc"];
        NSSortDescriptor *alphaDesc = [[NSSortDescriptor alloc] initWithKey:@"average" ascending:YES selector:@selector(compare:)];
        [mileageArray sortUsingDescriptors:[NSMutableArray arrayWithObjects:alphaDesc, nil]];	
        [alphaDesc release], alphaDesc = nil;
    } 
    [dtstartbutton setBackgroundImage:[UIImage imageNamed:@"date_start_deact.png"] forState:UIControlStateNormal];
    [dtendbutton setBackgroundImage:[UIImage imageNamed:@"date_end_deact.png"] forState:UIControlStateNormal];
    [costbutton setBackgroundImage:[UIImage imageNamed:@"cost_deact.png"] forState:UIControlStateNormal];
    [mileageTable reloadData];
    dtstartbutton.selected = NO;
    avgbutton.selected = YES;
    dtendbutton.selected = NO;
    costbutton.selected = NO;
}

- (void)costButtonTapped:(id)sender event:(id)event
{
    UIButton *avgbutton = [headerView.subviews objectAtIndex:0]; //avg button
    UIButton *dtendbutton = [headerView.subviews objectAtIndex:1]; //date end button
    UIButton *dtstartbutton = [headerView.subviews objectAtIndex:2]; //date start button
    UIButton *costbutton = [headerView.subviews objectAtIndex:4]; //cost button
    if(costbutton.tag == 8 || costbutton.tag == 8002)
    {
        costbutton.tag = 8001;
        [costbutton setBackgroundImage:[UIImage imageNamed:@"cost_act_down.png"] forState:UIControlStateNormal];
        //[self sortColumnButtonSelection:@"date" type:@"asc"];
        NSSortDescriptor *alphaDesc = [[NSSortDescriptor alloc] initWithKey:@"cost" ascending:NO selector:@selector(compare:)];
        [mileageArray sortUsingDescriptors:[NSMutableArray arrayWithObjects:alphaDesc, nil]];	
        [alphaDesc release], alphaDesc = nil;
    } else if(costbutton.tag == 8001) {
        costbutton.tag = 8002;
        [costbutton setBackgroundImage:[UIImage imageNamed:@"cost_act_up.png"] forState:UIControlStateNormal];
        //[self sortColumnButtonSelection:@"date" type:@"desc"];
        NSSortDescriptor *alphaDesc = [[NSSortDescriptor alloc] initWithKey:@"cost" ascending:YES selector:@selector(compare:)];
        [mileageArray sortUsingDescriptors:[NSMutableArray arrayWithObjects:alphaDesc, nil]];	
        [alphaDesc release], alphaDesc = nil;
    } 
    [avgbutton setBackgroundImage:[UIImage imageNamed:@"avg_deact.png"] forState:UIControlStateNormal];
    [dtendbutton setBackgroundImage:[UIImage imageNamed:@"date_end_deact.png"] forState:UIControlStateNormal];
    [dtstartbutton setBackgroundImage:[UIImage imageNamed:@"date_start_deact.png"] forState:UIControlStateNormal];
    [mileageTable reloadData];
    dtstartbutton.selected = NO;
    avgbutton.selected = NO;
    dtendbutton.selected = NO;
    costbutton.selected = YES;
}


- (void)viewDidLoad
{
    [super viewDidLoad];
    [Flurry logEvent:@"Mileage_Event"];
    activeSessionRowId = -1;
#ifdef ENABLE_BACK_BUTTON
    UIBarButtonItem *backButton = [[UIBarButtonItem alloc] initWithTitle:@"Back" style:UIBarButtonItemStylePlain target:nil action:nil];
    self.navigationItem.backBarButtonItem = backButton;
    [backButton release]; 
    
#endif
    #ifdef TESTING
    [TestFlight passCheckpoint:@"Mileage Button Pressed"];
    #endif 
    // Do any additional setup after loading the view from its nib.
    
    //infoBtn.frame = CGRectMake(infoBtn.frame.origin.x, self.mileageTable.frame.size.height+15, infoBtn.frame.size.width, infoBtn.frame.size.height);
    infoBtn.frame = CGRectMake(infoBtn.frame.origin.x, self.view.frame.size.height-30, infoBtn.frame.size.width, infoBtn.frame.size.height);
    
    UIBarButtonItem *typesaveButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemAdd target:self action:@selector(AddTypeSave:)];
    self.navigationItem.rightBarButtonItem = typesaveButton;
    [typesaveButton release];
    
    mileageTable.backgroundColor = [UIColor clearColor];
    
    mileageTable.separatorStyle = UITableViewCellSeparatorStyleNone;
    int currentVCIndex = [self.navigationController.viewControllers indexOfObject:self.navigationController.topViewController];
    DebugLog(@"parent %@",[self.navigationController.viewControllers objectAtIndex:currentVCIndex-1]);
    if([[self.navigationController.viewControllers objectAtIndex:currentVCIndex-1] isKindOfClass:[MyCarViewController class]])
    {
        DebugLog(@"Mycar controller");
        [self readCarIDFromDatabase];
    } else {
        DebugLog(@"CarUtilSecondViewController");
        self.cardId = 0;
    }
    [self readMileageFromDatabase];
    
//    NSSortDescriptor *alphaDesc = [[NSSortDescriptor alloc] initWithKey:@"startdate" ascending:NO selector:@selector(compare:)];
//    [mileageArray sortUsingDescriptors:[NSMutableArray arrayWithObjects:alphaDesc, nil]];
//    [alphaDesc release], alphaDesc = nil;
    
    //[self.mileageTable reloadData];

}

-(void) readCarIDFromDatabase {
    
    const char *dbpath = [[(CarUtilAppDelegate *)[[UIApplication sharedApplication] delegate] databasePath] UTF8String];
	
	// Open the database from the users filessytem
	if(sqlite3_open(dbpath, &expenseDB) == SQLITE_OK) {
		// Setup the SQL Statement and compile it for faster access
        NSString *querySQL = [NSString stringWithFormat:@"Select ID from MYCAR WHERE selected=\"%@\"", @"1"];
        const char *sqlStatement = [querySQL UTF8String];
		sqlite3_stmt *compiledStatement;
		if(sqlite3_prepare_v2(expenseDB, sqlStatement, -1, &compiledStatement, NULL) == SQLITE_OK) {
			// Loop through the results and add them to the feeds array
			while(sqlite3_step(compiledStatement) == SQLITE_ROW) {
				// Read the data from the result row
                self.cardId = sqlite3_column_int(compiledStatement, 0);
                DebugLog(@"car id -> %d",self.cardId);
            }
		}
		// Release the compiled statement from memory
		sqlite3_finalize(compiledStatement);
	}
	sqlite3_close(expenseDB);
}

-(void)viewDidAppear:(BOOL)animated 
{
    [self reloadAll];
    [self addTotalMileageAvg];
    [super viewDidAppear:animated];
}

-(void) readMileageFromDatabase {
    
// MILEAGE(ID INTEGER PRIMARY KEY AUTOINCREMENT,STARTDATE REAL, STARTKM REAL, ENDDATE REAL, ENDKM REAL,FUEL REAL, COST REAL, AVERAGE REAL, SELECTED TEXT, CARID INTEGER , TOTALKM REAL,FLAGTYPE INTEGER
    const char *dbpath = [[(CarUtilAppDelegate *)[[UIApplication sharedApplication] delegate] databasePath] UTF8String];
	
    
    // Init the expense Array
    if(mileageArray == nil) {
        mileageArray = [[NSMutableArray alloc] init];
    } else {
        [mileageArray removeAllObjects];
    }
	DebugLog(@"carid %d",self.cardId);
	// Open the database from the users filessytem
	if(sqlite3_open(dbpath, &expenseDB) == SQLITE_OK) {
		// Setup the SQL Statement and compile it for faster access
        NSString *querySQL = [NSString stringWithFormat:@"select ID,STARTDATE ,STARTKM,ENDDATE,ENDKM,AVGMILEAGE, COST, AVERAGE, SELECTED, FLAGTYPE  from MILEAGE where CARID=\"%d\"", self.cardId];
        const char *sqlStatement = [querySQL UTF8String];
		sqlite3_stmt *compiledStatement;
		if(sqlite3_prepare_v2(expenseDB, sqlStatement, -1, &compiledStatement, NULL) == SQLITE_OK) {
			// Loop through the results and add them to the feeds array
			while(sqlite3_step(compiledStatement) == SQLITE_ROW) {
				// Read the data from the result row
                NSInteger primaryKey = sqlite3_column_int(compiledStatement, 0);
				double startdateCol = sqlite3_column_double(compiledStatement, 1);
				double startreadingCol = sqlite3_column_double(compiledStatement, 2);
				double enddateCol = sqlite3_column_double(compiledStatement, 3);
                double endreadingCol = sqlite3_column_double(compiledStatement, 4);
                double avgMileageCol = sqlite3_column_double(compiledStatement, 5);
                double costCol = sqlite3_column_double(compiledStatement, 6);
                double avgCol = sqlite3_column_double(compiledStatement, 7);
                NSString *selectedCol = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatement, 8)];
                NSInteger flagTypeCol = sqlite3_column_int(compiledStatement, 9);

                if (flagTypeCol == START || flagTypeCol == INTERMEDIATE)
                    activeSessionRowId = primaryKey;
                else
                    activeSessionRowId = -1;
                DebugLog(@"flagTypeCol %d ",flagTypeCol);
                DebugLog(@"primaryKey %d -startdateCol %f -enddateCol %f -startreadingCol %f -endreadingCol %f - %f - %f - %f - %@",primaryKey, startdateCol, enddateCol, startreadingCol, endreadingCol, avgMileageCol, costCol, avgCol, selectedCol);
				// Create a new expense object with the data from the database
				MielageDetail *mileageObj = [[MielageDetail alloc] initWithID:primaryKey startdate:startdateCol enddate:enddateCol startreading:startreadingCol endreading:endreadingCol avgMileage:avgMileageCol average:avgCol cost:costCol selected:selectedCol flagType:flagTypeCol];
				// Add the animal object to the animals Array
				[mileageArray addObject:mileageObj];
				[mileageObj release];
			}
		}
		// Release the compiled statement from memory
		sqlite3_finalize(compiledStatement);
	}
	sqlite3_close(expenseDB);
}

- (void) DeleteMilegaeBreakReading :(int) mileageId
{   
    sqlite3_stmt    *statement;
    
    const char *dbpath = [[(CarUtilAppDelegate *)[[UIApplication sharedApplication] delegate] databasePath] UTF8String];
    
    if (sqlite3_open(dbpath, &expenseDB) == SQLITE_OK)
    {
        NSString *deleteSQL = [NSString stringWithFormat:@"DELETE FROM MILEAGE_BREAKS WHERE MILEAGEID=\"%d\"", mileageId];
        const char *delete_stmt = [deleteSQL UTF8String];
        sqlite3_prepare_v2(expenseDB, delete_stmt, -1, &statement, NULL);
        if (sqlite3_step(statement) == SQLITE_DONE)
        {
            //[self showMessage:@"Contact deleted"];
            //[self clearData:nil];
        } else {
            //[self showMessage:@"Failed to delete contact"];
        }
        sqlite3_finalize(statement);
    }         
    sqlite3_close(expenseDB);  
}

-(void)adddataInMileageArray:(NSInteger)lid  startdate:(double)sdate enddate:(double)edate startreading:(double)sreading endreading:(double)ereading fuel:(double)lfuel average:(double)lavg cost:(double)lcost selected:(NSString *)lselected
{
    MielageDetail *mileageObj = [[MielageDetail alloc] initWithID:lid startdate:sdate enddate:edate startreading:sreading  endreading:ereading fuel:lfuel average:lavg cost:lcost selected:lselected];
    // Add the animal object to the animals Array
    [mileageArray addObject:mileageObj];
    
    [mileageObj release];
    
    NSSortDescriptor *alphaDesc = [[NSSortDescriptor alloc] initWithKey:@"startdate" ascending:NO selector:@selector(compare:)];
    [mileageArray sortUsingDescriptors:[NSMutableArray arrayWithObjects:alphaDesc, nil]];	
    [alphaDesc release], alphaDesc = nil;
    
    [self.mileageTable reloadData];
}

-(void)reloadAll
{
    [mileageArray removeAllObjects];
    [self readMileageFromDatabase];
    [self sortMileageArraybyStartDate];
    [mileageTable reloadData];
}

- (IBAction)infoBtnPressed:(id)sender {
    
    UIAlertView *message = [[[UIAlertView alloc] initWithTitle:ALERT_TITLE
                                                       message:@"1. To add new Mileage, click on the + sign on top right.\n2. Enter the Odometer Reading i.e. the distance travelled by your vehicle in Km.\n3. Set the date of your travel. By default, it is the current date.\n4. Enter the Fuel consumed in litres.\n5.  If the tank is partially filled, check the box \"Partial Fillup\". To know more click on (?) button.\n6. Enter the cost incurred. Save.\n7. To delete the entry, slide the entry to the left and press Delete.\n8. Tap the entry to add new data.\n9. Tap on the 'Calculate Mileage' button and enter the new odometer reading & the End Date.\n\nNote: If you forget to enter any past fuel details in the app, do not continue adding the data in the current session. The mileage calculation may not be accurate. End the session and start with a new entry thereafter."
                                                      delegate:nil
                                             cancelButtonTitle:@"OK"
                                             otherButtonTitles:nil] autorelease];
    
    [message show];
}

- (void) AddTypeSave: (id) sender
{
//    AddMileageViewController *addmileageviewController = [[AddMileageViewController alloc] initWithNibName:@"AddMileageViewController" bundle:nil] ;
//    addmileageviewController.title = @"Mileage Meter";
//    addmileageviewController.cardId = self.cardId;
//    addmileageviewController.dbowID = -1;
//    addmileageviewController.rowID = -1;
//    //to push the UIView.
//    addmileageviewController.hidesBottomBarWhenPushed = YES;
//    [self.navigationController pushViewController:addmileageviewController animated:YES];
//    [addmileageviewController release];
    
    //if ([self getMileageBreakRowCountFromDatabase] != 0)
    DebugLog(@"activesessionid %d && carid %d",activeSessionRowId,self.cardId);
    if (activeSessionRowId != -1)
    {
        FuelDetailViewController *fueldetailController = [[FuelDetailViewController alloc] initWithNibName:@"FuelDetailViewController" bundle:nil] ;
        fueldetailController.title = @"Fuel Details";
        fueldetailController.cardId = self.cardId;
        fueldetailController.dbowID = self.activeSessionRowId;
        fueldetailController.hidesBottomBarWhenPushed = NO;
        [self.navigationController pushViewController:fueldetailController animated:YES];
        [fueldetailController release];
    }
    else
    {
        AddEcditFuelViewController *addfueldetailsController = [[AddEcditFuelViewController alloc] initWithNibName:@"AddEcditFuelViewController" bundle:nil] ;
        addfueldetailsController.hidesBottomBarWhenPushed = NO;
        addfueldetailsController.headerImageType = START;
        addfueldetailsController.carId = self.cardId;
        [self.navigationController pushViewController:addfueldetailsController animated:YES];
        [addfueldetailsController release];
    }
    
}
//-(NSInteger) getMileageBreakRowCountFromDatabase
//{
//    DebugLog(@"reading mileage row count");
//    //MILEAGE_BREAKS(ID INTEGER PRIMARY KEY AUTOINCREMENT, MILEAGEID INTEGER, DATE REAL, FUEL REAL, COST REAL)
//    //MILEAGE_BREAKS1(ID INTEGER PRIMARY KEY AUTOINCREMENT, MILEAGEID INTEGER,ODOMETER_READING REAL, DATE REAL, FUEL REAL,ISPARTIAL INTEGER, COST REAL, CARID INTEGER)
//    NSInteger rowCount = 0;
//    if(sqlite3_open([[NEONAppDelegate databasePath] UTF8String], &expenseDB) == SQLITE_OK)
//    {
//        NSString *queryString = @"select count (*) from MILEAGE_BREAKS1";
//        const char *queryCharP = [queryString UTF8String];
//        sqlite3_stmt *sqlStatement = nil;
//        if (sqlite3_prepare_v2(expenseDB, queryCharP, -1, &sqlStatement, NULL) == SQLITE_OK)
//        {
//            if (sqlite3_step(sqlStatement) == SQLITE_ROW)
//            {
//                rowCount = sqlite3_column_int(sqlStatement, 0);
//            }
//        }
//        sqlite3_finalize(sqlStatement);
//        sqlite3_close(expenseDB);
//    }
//    DebugLog(@"got mileage row count as %d",rowCount);
//    return rowCount;
//}
- (void)viewDidUnload
{
    self.headerView = nil;
    self.footerView = nil;
    [self setMileageTable:nil];
    [self setInfoBtn:nil];
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

-(BOOL)shouldAutorotate
{
    return NO;
}

-(NSUInteger)supportedInterfaceOrientations
{
    return UIInterfaceOrientationMaskPortrait;
}

- (void)dealloc {
    //[Flurry endTimedEvent:@"Mileage_Event" withParameters:nil];
    if(headerView != nil)
        [headerView release];
    if(footerView != nil)
        [footerView release];
    [mileageArray release];
    [mileageTable release];
    [infoBtn release];
    [super dealloc];
}
@end
