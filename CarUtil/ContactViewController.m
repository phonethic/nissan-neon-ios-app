//
//  ContactViewController.m
//  CarUtil
//
//  Created by Rishi Saxena on 09/04/12.
//  Copyright (c) 2012 Phonethics. All rights reserved.
//

#import "ContactViewController.h"
#import "BreakdownLocationViewController.h"
#import "CarUtilAppDelegate.h"
#import "CarUtilWebViewController.h"
#import "CarUtilStoreListViewController.h"

@interface ContactViewController ()

@end

@implementation ContactViewController
@synthesize contactTableView;
@synthesize bgImageView;
@synthesize contactList;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        self.navigationItem.title = NSLocalizedString(@"Contact", @"Contact");
        //self.tabBarItem.image = [UIImage imageNamed:@"contact"];
    }
    return self;
}
- (void)viewDidLoad
{
#ifdef ENABLE_BACK_BUTTON
    UIBarButtonItem *backButton = [[UIBarButtonItem alloc] initWithTitle:@"Back" style:UIBarButtonItemStylePlain target:nil action:nil];
    self.navigationItem.backBarButtonItem = backButton;
    [backButton release];
    
#endif
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    contactList = [[NSArray alloc] initWithObjects:TESTDRIVE_TEL_TEXT,CUSTSUPPORT_TEL_TEXT,CUSTSUPPORT_EMAIL_TEXT,BREAKDOWN_TEL_TEXT,BREAKDOWN_EMAIL_TEXT,FACEBOOK_TEXT,DEALER_TEXT, nil];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    #ifdef TESTING
    [TestFlight passCheckpoint:@"Contact Tab Pressed"];
    #endif
}

- (void)viewDidUnload
{
    [self setContactTableView:nil];
    [self setContactList:nil];
    [self setBgImageView:nil];
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (void)dealloc {
    [contactTableView release];
    [contactList release];
    [bgImageView release];
    [super dealloc];
}


- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return YES;
}

-(BOOL)shouldAutorotate
{
    return NO;
}

-(NSUInteger)supportedInterfaceOrientations
{
    return UIInterfaceOrientationMaskPortrait;
}

#pragma mark Table view methods
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    CGRect screenBounds = [[UIScreen mainScreen] bounds];
    if (screenBounds.size.height == 568) {
        // code for 4-inch screen
        return 65;
    } else {
        // code for 3.5-inch screen
        return 61.5;
    }
    return 61.5;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [contactList count];;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    static NSString *CellIdentifier = @"Cell";
    UILabel *lblText;
    NSString *textString = [contactList objectAtIndex:indexPath.row];
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier] autorelease];
        
        lblText = [[UILabel alloc] initWithFrame:CGRectMake(cell.frame.origin.x + 60 , cell.frame.origin.y + 20, 250, cell.frame.size.height - 20)];
        lblText.tag = 1;
        lblText.textColor = [UIColor whiteColor];
        lblText.font = [UIFont boldSystemFontOfSize:17.0];
        lblText.shadowColor = [UIColor blackColor];
        lblText.shadowOffset = CGSizeMake(0, 1);
        lblText.backgroundColor = [UIColor clearColor];
        lblText.textAlignment = UITextAlignmentLeft;
        [cell.contentView addSubview:lblText];
        [lblText release];
    }
    lblText = (UILabel *)[cell viewWithTag:1];
    lblText.text = textString;
    
    UIImageView *bg = [[UIImageView alloc] initWithFrame:cell.frame];
    UIImageView *bgon = [[UIImageView alloc] initWithFrame:cell.frame];
    if ([textString isEqualToString:TESTDRIVE_TEL_TEXT] || [textString isEqualToString:CUSTSUPPORT_TEL_TEXT] || [textString isEqualToString:BREAKDOWN_TEL_TEXT])
    {
        bg.image = [UIImage imageNamed:@"call_off.png"];
        bgon.image = [UIImage imageNamed:@"call_on.png"];
    }
    else if ([textString isEqualToString:CUSTSUPPORT_EMAIL_TEXT])
    {
        bg.image = [UIImage imageNamed:@"email_off.png"];
        bgon.image = [UIImage imageNamed:@"email_on.png"];
    }
    else if ([textString isEqualToString:BREAKDOWN_EMAIL_TEXT]|| [textString isEqualToString:DEALER_TEXT])
    {
        bg.image = [UIImage imageNamed:@"location_off.png"];
        bgon.image = [UIImage imageNamed:@"location_on.png"];
    }
    else if ([textString isEqualToString:FACEBOOK_TEXT])
    {
        bg.image = [UIImage imageNamed:@"fb_off.png"];
        bgon.image = [UIImage imageNamed:@"fb_on.png"];
    }
    cell.backgroundView = bg;
    [bg release];
    cell.selectedBackgroundView = bgon;
    [bgon release];

    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *selectedText =  [contactList objectAtIndex:indexPath.row];
    if ([selectedText isEqualToString:TESTDRIVE_TEL_TEXT])
    {
        [self bookTestDriveBtnPressed];
    }
    else if ([selectedText isEqualToString:CUSTSUPPORT_TEL_TEXT])
    {
        [self callCustSupportBtnPressed];
    }
    else if ([selectedText isEqualToString:CUSTSUPPORT_EMAIL_TEXT])
    {
        [self emailCustSupportBtnPressed];
    }
    else if ([selectedText isEqualToString:BREAKDOWN_TEL_TEXT])
    {
        [self callBreakdownBtnPressed];
    }
    else if ([selectedText isEqualToString:BREAKDOWN_EMAIL_TEXT])
    {
        [self sendBreakdownLocationbtnPressed];
    }
    else if ([selectedText isEqualToString:FACEBOOK_TEXT])
    {
        if([NEONAppDelegate networkavailable])
        {
            [self openWebSitebtnPressed];
        }
        else
        {
            UIAlertView *errorView = [[[UIAlertView alloc]
                                       initWithTitle: ALERT_TITLE
                                       message:@"Please check your internet connection and try again."
                                       delegate:self
                                       cancelButtonTitle:@"OK"
                                       otherButtonTitles:nil] autorelease];
            [errorView show];
        }
    }
    else if ([selectedText isEqualToString:DEALER_TEXT])
    {
        [self storeLocatebtnPressed];
    }
     [tableView deselectRowAtIndexPath:indexPath animated:YES];
}

#pragma mark internal methods
- (void)bookTestDriveBtnPressed
{
    NSString *deviceType = [UIDevice currentDevice].model;
    if([deviceType isEqualToString:@"iPhone"])
    {
        UIAlertView *alert = [ [ UIAlertView alloc ] initWithTitle: ALERT_TITLE
                                                           message: @"Do you want to call Test Drive center number ?"
                                                          delegate: self
                                                 cancelButtonTitle: nil
                                                 otherButtonTitles: @"NO", @"YES", nil
                              ];
        [alert setTag:1];
        [ alert show ];
        [alert release];
    } else {
        UIAlertView *alert = [ [ UIAlertView alloc ] initWithTitle: ALERT_TITLE
                                                           message: @"Calling functionaltiy is not available in this device. "
                                                          delegate: self
                                                 cancelButtonTitle: nil
                                                 otherButtonTitles: @"OK", nil
                              ];
        [ alert show ];
        [alert release];
    }
}

- (void)callCustSupportBtnPressed
{
    NSString *deviceType = [UIDevice currentDevice].model;
    if([deviceType isEqualToString:@"iPhone"])
    {
        UIAlertView *alert = [ [ UIAlertView alloc ] initWithTitle: ALERT_TITLE
                                                           message: @"Do you want to call our Customer Care ?"
                                                          delegate: self
                                                 cancelButtonTitle: nil
                                                 otherButtonTitles: @"NO", @"YES", nil
                              ];
        [alert setTag:2];
        [ alert show ];
        [alert release];
    } else {
        UIAlertView *alert = [ [ UIAlertView alloc ] initWithTitle: ALERT_TITLE
                                                           message: @"Calling functionaltiy is not available in this device. "
                                                          delegate: self
                                                 cancelButtonTitle: nil
                                                 otherButtonTitles: @"OK", nil
                              ];
        [ alert show ];
        [alert release];
    }
}

- (void)emailCustSupportBtnPressed
{
    @try{
        MFMailComposeViewController *picker=[[[MFMailComposeViewController alloc]init] autorelease];
        picker.mailComposeDelegate=self;
        picker.navigationBar.tintColor = [UIColor blackColor];
        [picker setToRecipients:[NSArray arrayWithObject:CUSTSUPPORT_EMAILID]];
        [picker setSubject:@"Enquiry for NISSAN"];
        [self presentModalViewController:picker animated:YES];
    }
    @catch (NSException *exception)
    {
        //DebugLog(@"Exception %@",exception.reason);
    }
}

- (void)callBreakdownBtnPressed
{
    NSString *deviceType = [UIDevice currentDevice].model;
    if([deviceType isEqualToString:@"iPhone"])
    {
        UIAlertView *alert = [ [ UIAlertView alloc ] initWithTitle: ALERT_TITLE
                                                           message: @"Do you want to call our Helpline number ?"
                                                          delegate: self
                                                 cancelButtonTitle: nil
                                                 otherButtonTitles: @"NO", @"YES", nil
                              ];
        [alert setTag:3];
        [alert show ];
        [alert release];
    } else {
        UIAlertView *alert = [ [ UIAlertView alloc ] initWithTitle: ALERT_TITLE
                                                           message: @"Calling functionaltiy is not available in this device. "
                                                          delegate: self
                                                 cancelButtonTitle: nil
                                                 otherButtonTitles: @"OK", nil
                              ];
        [ alert show ];
        [alert release];
    }
}

- (void)sendBreakdownLocationbtnPressed
{
    BreakdownLocationViewController *breakdownLocViewController = [[BreakdownLocationViewController alloc] initWithNibName:@"BreakdownLocationViewController" bundle:nil] ;
    breakdownLocViewController.title = BREAKDOWN_EMAIL_TEXT;
    breakdownLocViewController.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:breakdownLocViewController animated:YES];
    [breakdownLocViewController release];
}


- (void)openWebSitebtnPressed
{
    CarUtilWebViewController *webSiteViewController = [[CarUtilWebViewController alloc] initWithNibName:@"CarUtilWebViewController" bundle:nil] ;
    webSiteViewController.title = @"Facebook";
    webSiteViewController.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:webSiteViewController animated:YES];
    [webSiteViewController release];
}
- (void)storeLocatebtnPressed
{
    CarUtilStoreListViewController *storeLocListViewController = [[CarUtilStoreListViewController alloc] initWithNibName:@"CarUtilStoreListViewController" bundle:nil] ;
    storeLocListViewController.title = DEALER_TEXT;
    storeLocListViewController.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:storeLocListViewController animated:YES];
    [storeLocListViewController release];
}
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
#ifdef GOOGLE_ANALYTICS
    if (![[GANTracker sharedTracker] trackEvent:@"Nissan CarUitl Application iOS"
                                         action:@"Contact Number Dialed"
                                          label:@""
                                          value:-1
                                      withError:NULL]) {
        DebugLog(@"error in trackEvent");
    }
#endif
    if (buttonIndex == 1)
    {
        NSString *phoneNumber = nil;
        switch (alertView.tag) {
            case 1:
            {
                phoneNumber = TESTDRIVE_TEL;
                [Flurry logEvent:@"TestDrive_Call_Event"];
            }
                break;
            case 2:
            {
                phoneNumber = CUSTSUPPORT_TEL;
                [Flurry logEvent:@"CustomerSupport_Call_Event"];
            }
                break;
            case 3:
            {
                phoneNumber = BREAKDOWN_TEL;
                [Flurry logEvent:@"Breakdown_Call_Event"];
            }
                break;
            default:
                break;
        }
        UIApplication *myApp = [UIApplication sharedApplication];
        [myApp openURL:[NSURL URLWithString:phoneNumber]];
        return;
    }
}

-(void)mailComposeController:(MFMailComposeViewController *)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError *)error{
     if (result == MFMailComposeResultSent)
     {
         [Flurry logEvent:@"Customer_Email_Event"];
     }
    [controller dismissModalViewControllerAnimated:YES];
}
@end
