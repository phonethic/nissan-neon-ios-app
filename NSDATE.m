//There doesn't appear to be a built-in mechanism for doing this, so we rolled
//a set of categories on NSDate that I don't mind sharing.
//Note that the string constructor has a parameter for capitalization because
//we use it both to label (action timestamps, not objects, in our case)
//implicitly ("Yesterday at 12:31") and as part of a larger string ("Last
//updated today at 12:45"). We also include the time for days prior to today
//in our usage, but you can of course adapt to your needs. And we force the
//day-of-week names to English and localize them with NSLocalizedString,
//instead of letting the NSDateFormatter create a localized day name, so that
//we only get localized day names for the same locales where we've provided
//translations for "Today" and "Yesterday".
//Cheers,
//-Bryce
                                                                  
@implementation NSDate (NaturalDates)
- (BOOL)isSameDay:(NSDate*) anotherDate
{
    NSCalendar* calendar = [NSCalendar currentCalendar];
    NSDateComponents* components1 = [calendar components:(NSYearCalendarUnit
                                                          | NSMonthCalendarUnit | NSDayCalendarUnit) fromDate:self];
    NSDateComponents* components2 = [calendar components:(NSYearCalendarUnit
                                                          | NSMonthCalendarUnit | NSDayCalendarUnit) fromDate:anotherDate];
    return ([components1 year] == [components2 year] && [components1 month]
            == [components2 month] && [components1 day] == [components2 day]);
}
- (BOOL)isToday
{
    return [self isSameDay:[NSDate date]];
}
- (BOOL)isYesterday
{
    NSCalendar* calendar = [NSCalendar currentCalendar];
    NSDateComponents *comps = [[NSDateComponents alloc] init];
    [comps setDay:-1];
    NSDate *yesterday = [calendar dateByAddingComponents:comps
                                                  toDate:[NSDate date]  options:0];
    [comps release];
    return [self isSameDay:yesterday];
}
- (BOOL)isLastWeek
{
    NSCalendar* calendar = [NSCalendar currentCalendar];
    NSDateComponents *comps = [calendar
                               components:NSWeekCalendarUnit| NSDayCalendarUnit fromDate:[NSDate date]
                               toDate:self options:0];
    NSInteger week = [comps week];
    NSInteger days = [comps day];
    return (0==week && days<=0);
}
- (NSString *)stringFromDateCapitalized:( BOOL)capitalize;
{
    NSString *label = nil;
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    NSString *dateFormatPrefix = nil;
    [dateFormatter setDateStyle: NSDateFormatterNoStyle]; // Will display
    hour only, we are building the day ourselves
    [dateFormatter setTimeStyle: NSDateFormatterShortStyle];
    if([self isToday]) {
        if(capitalize) dateFormatPrefix = NSLocalizedString(@"Today at",
                                                            @"");
        else dateFormatPrefix = NSLocalizedString(@"today at", @"");
    }
    else if([self isYesterday]) {
        if(capitalize) dateFormatPrefix = NSLocalizedString(@"Yesterday at",
                                                            @"");
        else dateFormatPrefix = NSLocalizedString(@"yesterday at", @"");
    }
    else if([self isLastWeek]) {
        NSDateFormatter *weekDayFormatter = [[NSDateFormatter alloc] init];
        
        // We will set the locale to US to have the weekday in english.
        // The NSLocalizedString( weekDayString, @"") below will make it
        localized.
        NSLocale *locale = [[NSLocale alloc]
                            initWithLocaleIdentifier:@"en_ US"];
        [weekDayFormatter setLocale:locale];
        [locale release];
        [weekDayFormatter setDateFormat:@"EEEE"];
        NSString *weekDayString = [NSString stringWithFormat:@"%@ at",
                                   [weekDayFormatter stringFromDate:self]];
        dateFormatPrefix = NSLocalizedString( weekDayString, @"");
        [weekDayFormatter release];
    }
    else
    {
        [dateFormatter setDateStyle: NSDateFormatterShortStyle]; // Display
        the date as well
    }
    
    if(dateFormatPrefix != nil) { // We have a day string, add hour only
        label = [NSString stringWithFormat:@"%@ %@", dateFormatPrefix,
                 [dateFormatter stringFromDate:self]];
    }
    else { // Use the full date
        label = [dateFormatter stringFromDate:self];
    }
    [dateFormatter release];
    return label;
}
@end
